General notes
-------------
I mention the versions of software installed on my PC, but feel free to install newer (stable) versions if present, unless specified otherwise.

Install Git
-----------
Git is a Distributed version control system. Documentation: http://git-scm.com/documentation

Download the version for your OS from here http://git-scm.com/downloads (I'm using 1.8.3 for Windows)
At the end of the installation process, you will have Git Bash and Git GUI installed in your Windows Start menu.
I'm using Git's command line, so the commands in the following sections are run through  

Install Java
------------
This is required for Eclipse (next step).
An installation of some JRE version is most probably present on your PC, in which case you can skip this step.
If you do not have JDK/JRE installed, download and install it from http://www.oracle.com/technetwork/java/javase/downloads/index.html
I currently have JDK 1.7.0_10 installed on my PC, but a JRE installation is supposed to do fine too.

Install Python
--------------
Make sure you download python 2.6.x and not newer versions.

If you are using Windows too, then the latest version for which there is also a Windows installer can be found here:
http://www.python.org/download/releases/2.6.6/

Download the Windows MSI Installer suitable for your PC (32-bit or 64-bit) and run it.

Install Eclipse
---------------
Download Eclipse from here: http://www.eclipse.org/downloads/
I'm using Juno (4.2), latest version is Kepler (4.3). 

~~~~~~~~~

Install Eclipse Python plug-in
------------------------------
http://pydev.org/updates

Fetch the code
--------------
The main code repository is hosted in https://bitbucket.org/
It is a private repository, so please send me the email you want to use and I will send you an invitation.

Once you have Git installed and have permissions to access the repository, goto the workspace directory and clone the code:
$ cd ~/workspace/
$ git clone ...

Install MySQL
-------------
Download MySQL installer (mine was 5.6.13, Windows, 32-bit) and install SQL server and client
Add mysql.exe directory to Path environment variable, in my case C:\Program Files\MySQL\MySQL Server 5.6\bin\

MySQL - on Windows 7:
(django_foodcoop) C:\Users\ophir>c:\Users\ophir\virtualenv\django_foodcoop\Scripts\easy_install.exe file://c:/users/ophir/Downloads/MySQL-python-1.2.2
.win32-py2.6.exe
Processing MySQL-python-1.2.2.win32-py2.6.exe
creating 'c:\users\ophir\appdata\local\temp\easy_install-r1jfqj\MySQL_python-1.2.2-py2.6-win32.egg' and adding 'c:\users\ophir\appdata\local\temp\easy
_install-r1jfqj\MySQL_python-1.2.2-py2.6-win32.egg.tmp' to it
Moving MySQL_python-1.2.2-py2.6-win32.egg to c:\users\ophir\virtualenv\django_foodcoop\lib\site-packages
Adding MySQL-python 1.2.2 to easy-install.pth file

Installed c:\users\ophir\virtualenv\django_foodcoop\lib\site-packages\mysql_python-1.2.2-py2.6-win32.egg
Processing dependencies for MySQL-python==1.2.2
Finished processing dependencies for MySQL-python==1.2.2

Install Python dependencies
---------------------------
It is recommended to use virtualenv. Download and install virtualenv and then install all of the python dependencies like so:

$ mkdir ~/virtualenv # or use a different location if you wish
$ cd ~/virtualenv/
$ virtualenv django_foodcoop
[snip]
$ cd ~/workspace/django_foodcoop/ # the location of the project code - could be a different one
$ source ~/virtualenv/django_foodcoop/bin/activate
$ git pull origin master # to update the code
[snip]
$ pip install -r requirements.txt
[snip]
$ pip install -r requirements-linux.txt # or requirements-windows.txt - depending on your OS
[snip]

Run tests
---------

Now that we are supposed to have all the code in place, let's try to run the test suite:

$ cd wsgi/django_foodcoop/
$ ./manage.py test foodcoop
[snip]

Needless to say, all tests (currently 34) should pass.

Create DB
---------

$ mysql -uroot -p*****
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 2
Server version: 5.1.69 Source distribution

Copyright (c) 2000, 2013, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> create user 'foodcoop'@'localhost' identified by 'foodcoop';
Query OK, 0 rows affected (0.00 sec)

mysql> create database foodcoop;
Query OK, 1 row affected (0.00 sec)

mysql> grant all privileges on foodcoop.* to foodcoop@localhost;
Query OK, 0 rows affected (0.00 sec)

Run a local server
------------------
Currently, the easiest way to have a working DB copy is to load production's snapshot to local server:

$ export DJANGO_SETTINGS_MODULE=django_foodcoop.settings.dev_mysql
$ ./manage.py dbshell < ~/Dropbox/Backup/production/db-snapshots/2013-09-30-002519.sql 
[snip]

It is also useful when debugging problems on production.
Once we have data in our DB, run the local server:
$ ./manage.py runserver

And go to http://localhost:8000/ on your browser of choice

More to come
------------

$ ./manage.py syncdb --noinput
[snip]
(django_foodcoop)[pmophir@ophir-pc django_foodcoop]$ ./manage.py migrate django_extensions
[snip]
(django_foodcoop)[pmophir@ophir-pc django_foodcoop]$ ./manage.py migrate reversion
[snip]
(django_foodcoop)[pmophir@ophir-pc django_foodcoop]$ ./manage.py migrate foodcoop
[snip]
 
(django_foodcoop)[pmophir@ophir-pc django_foodcoop]$ ./manage.py dbshell < ../../scripts/drop-foodcoop-tables.sql 
