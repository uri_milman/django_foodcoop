Deploy
======
Production is using MySQL.

First deploy:

cartridge_type="python-2.6"
source $OPENSHIFT_HOMEDIR/$cartridge_type/virtenv/bin/activate
cd $OPENSHIFT_REPO_DIR
export DJANGO_SETTINGS_MODULE='django_foodcoop.settings.production'
python wsgi/django_foodcoop/manage.py syncdb --noinput
python wsgi/django_foodcoop/manage.py createsuperuser --username=admin --email=firtzel@gmail.com
#Password: R********
python wsgi/django_foodcoop/manage.py loaddata wsgi/fixtures/site.json
python wsgi/django_foodcoop/manage.py loaddata wsgi/fixtures/group.json
python wsgi/django_foodcoop/manage.py loaddata wsgi/fixtures/unit.json

#export DJANGO_SETTINGS_MODULE='django_foodcoop.settings.production'
#python wsgi/django_foodcoop/manage.py shell

#import os
#os.environ['DJANGO_SETTINGS_MODULE'] = 'django_foodcoop.settings.dev'
#from django.conf import settings as s
#s.DATABASES

Helpful commands for coopsandbox:

ssh 517441a5e0b8cd93230001f8@coopsandbox-rehovot.rhcloud.com

cartridge_type="python-2.6"
source $OPENSHIFT_HOMEDIR/$cartridge_type/virtenv/bin/activate
cd $OPENSHIFT_REPO_DIR
export DJANGO_SETTINGS_MODULE='django_foodcoop.settings.sandbox'
#python wsgi/django_foodcoop/manage.py dbshell < scripts/drop-foodcoop-tables.sql
