2013-04-24
----------
Initial release to production, should be used by purchasing managers

2013-04-30
----------
Second release, additional users are sale day managers

changes:
* foodcoop application is now managed by South.
* all DB changes are included in migrations 0002_xxx and onwards need to be applied to production

On sandbox, load new data from fixtures, including members who are sale day managers:
manage.py initdata

2013-05-19
----------
Views for treasury managements, and almost for members

Sandbox:

	syncdb --noinput
	migrate django_extensions --list
	migrate reversion --list
	migrate foodcoop --list
	loaddata wsgi/fixtures/flatpage.json

Production:

	pip install -r requirements.txt
	syncdb --noinput
	migrate django_extensions --list
	migrate django_extensions
	migrate reversion --list
	migrate reversion
	migrate foodcoop --list
	migrate foodcoop 0001 --fake
	migrate foodcoop
	loaddata wsgi/fixtures/flatpage.json
	collectstatic

2013-05-20
----------
* Member order: added some fields - min_amount, max_amount, comments 
* Final fixes before opening sale for member orders
* Extra flat page for sale announcement

2013-05-22
----------
Due to some DB changes, do the following:
    
    loaddata flatpage.json group.json

2013-05-23
----------
    loaddata flatpage.json

And then manually open first sale via admin page (/admin/foodcoop/sale/1/)

2013-05-29
----------
Bug fixes

2013-06-01
----------
*   migration 0006 - added pre/post sale comments to sale products
*   added a couple of views for purchase managers

2013-06-02
----------
*   updated prices according to the VAT raise (from 17% to 18%) using scripts/fix-prices-due-to-VAT-change.py
*   migration 0007 - added VAT prices to sale products
*   updated new VAT price fields according to prices from Product using scripts/copy-prices-from-products-to-sale-products.py 

2013-06-08
----------
Pushed minor fixes to existing views

2013-06-09
----------
*   migration 0008 - added a couple of fields to SaleProduct to store total amount ordered
    (to be filled automatically when the sale is finalized) and actual amount - to be fixed manually after the sale
    due to last minute changes in amount sent to us by the supplier.  
*   all is now ready to finalize sale

2013-06-11
----------
Fix for the following bug:

The "amount" field in ProductFinalOrder was used for two different purposes, instead of having two distinct fields:

First purpose - store the final amount which was supposed to be picked up by the member

Second purpose - store the actual amount bought by the member during sale day

The former was overridden by the latter during sale day, and made us lose the much needed information on the
amount ordered from the supplier
  
*   migration 0009 - added field "amount_bought" to ProductFinalOrder
*   migration 0010 - added field "amount_leftover" to SaleProduct
*   script recover-final-amounts.py was used manually to recover date 

How to perform fix on production?
1.  perform migrations 0009 and 0010
2.  execfile('scripts/recover-final-amounts.py') from within shell_plus, and then execute "main()"

2013-06-17
----------
Truncate ProductOrder fields "amount", "min_amount" and "max_amount" to DecimalField without any decimal digits.

Migration requires:
1.  execfile('scripts/query-member-orders.py') which will truncate existing problematic orders
2.  perform migration 0011
 
2013-06-23
----------
Misc. fixes, mostly small ones.

Migration requires:
1.  git push productioncoop
2.  Go to URL /foodcoop/actions/sale/2/draft/
3.  fix bug in prices:
execfile('scripts/copy-prices-from-products-to-sale-products.py')
fix_sale_prices(sale_id=2)
4.  fix bug in round rice:
execfile('scripts/create-new-product-orders-to-july-sale.py')

2013-07-01
----------
1.  add package openpyxl by running: pip install -r requirements.txt
2.  migration 0012 - add "was_bought" field

2013-07-06
----------
Including: archive pages for treasury, and some bug fixes.
 
1.  migration 0013 - add "delivery_amount" field

2013-07-xx
----------
Perform calculations for sum paid by member during sale dynamically (requires DB changes)

1.  migration 0014 - add "sale_product" field to ProductFinalOrder
    CAUTION: the next migration (0015) has to be done separately, since we need to first populate the new field.
2.  script to copy field value from ProductOrder to ProductFinalOrder
3.  migration 0015 - change "sale_product" to a required field

2013-07-14
----------
flatpages with RTF editor (TinyMCE)

1.  add package django-tinymce by running: pip install -r requirements.txt

2013-07-22
----------
Minor features, fix annoying bug when finalizing user orders and entering invalid (non-numeric) amount

1.  execfile('scripts/add-new-products-to-august-2013-sale.py')
and run the following commands:
clear_admin_orders(sale_id=3) # no need for orders from user 'admin'
create_new_product_orders(sale_id=3, product_id=250) # yellow lentils - the only problematic non-packaged product

2.  http://foodcoop-rehovot.rhcloud.com/foodcoop/actions/sale/3/draft/

2013-07-27
----------
DB change - added categories to suppliers, to be able to split Hertzl Bibi's products into groups

1.  migration 0016

2013-08-03
----------
Start using the new category attribute, added PaymentToSupplier model which will be used to make some 
sense for treasury persons

1.  migration 0017

2013-08-10
----------
add some features, mainly for administrators (no model changes)

2013-09-12
----------
Ran the following commands via shell_plus to change package size of non-packaged products from 25 kg to 24 kg:
 
Product.objects.filter(supplier=4, package_size=25).update(package_size=24)
SaleProduct.objects.filter(product__supplier=4, sale=5, package_size=25).update(package_size=24)

2013-09-16
----------
Updated code on sandbox, copied data from production and messed with the data using the following new command:

    ./manage.py sandboxdata

2013-12-10
----------
During South migration 0018 I (Ophir) tried to add a new table foodcoop_otherpayment, but while running the South
migration the collation for the table was set to "latin1_swedish_ci" instead of "utf8_general_ci".

I browsed the web for some pointers, and finally found this workaround:
1.  test the migration on my PC, and once ready - commit the changes and deploy to production
2.  extract the CREATE TABLE command using:
    python manage.py migrate --db-dry-run --verbosity=2
3.  run the same command on production, adding "DEFAULT CHARACTER SET=utf8" to the statement, like so:

    CREATE TABLE `foodcoop_otherpayment` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
                                          `sum` numeric(10, 2) NOT NULL,
                                          `expense` bool NOT NULL,
                                          `date` date NULL,
                                          `description` longtext NOT NULL,
                                          `comments` longtext NOT NULL) DEFAULT CHARACTER SET=utf8;
4. verify that the collation is indeed "utf8_general_ci" by running:
    SHOW TABLE STATUS;

