Creating a new sale
-------------------
Via admin page, as "open to buyers", meaning to purchasing managers to select the products which will be sold in this sale.

Opening the sale
----------------
http://localhost:8000/foodcoop/actions/sale/1/open/
This will open the sale to the "public", meaning members who will be able to place their orders

Sale draft
----------
For example:
http://localhost:8000/foodcoop/actions/sale/1/close/

This will move the sale to "draft" status, ...

