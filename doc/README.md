django_foodcoop
===============

Tasks are managed at https://trello.com/board/django-foodcoop/5107e85ee1831feb25004386

Backup
------
Follow advice here:
https://www.openshift.com/kb/kb-e1047-how-do-i-backup-and-restore-my-openshift-data

And specifically, run:
rhc snapshot save -a foodcoop

Production
----------
Hosted on OpenShift, DB is MySQL. See doc/DEPLOY.md

Sandbox
-------
Hosted on OpenShift, DB is sqlite.
Address: http://coopsandbox-rehovot.rhcloud.com/
Remote access: ssh 517441a5e0b8cd93230001f8@coopsandbox-rehovot.rhcloud.com

Using South
-----------
After changing one or more of the models in foodcoop/models.py, run the following commands:

# manage.py schemamigration foodcoop --auto
# manage.py migrate foodcoop

pinax-project-account
---------------------
This project was integrated with pinax-project-account. First integration was done at 2013/04/15

Debugging
---------
Installed https://github.com/django-debug-toolbar/django-debug-toolbar

Fixtures
--------
Used for both development and unit testing. To upload the fixtures listed in utils.FIXTURES to dev DB,
do the following:
 
rm dev.db
./manage initdata

Creating fixtures is details in FIXTURES.md

Unit testing
------------
There are some, using fixtures listed in utils.TEST_FIXTURES.
Running them is easy:

./manage.py test foodcoop
 

Forms for sets
--------------
Integrating the following projects:
https://github.com/AndrewIngram/django-extra-views
http://code.google.com/p/django-dynamic-formset/

Tables
------
Used http://www.datatables.net/ for example for listing supplier products
