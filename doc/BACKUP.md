Backup
======

Snapshots
---------
Run the following OpenShift command to take a snapshot of the application:

$ cd ~/Backup/coopsandbox/2013-05-04
$ rhc snapshot save <appname>

Database
--------
Use ssh + mysqldump to get a DB snapshot (running on my other laptop)

IMPORTANT: use port forwarding while doing this
https://www.openshift.com/blogs/getting-started-with-port-forwarding-on-openshift

Source code
-----------
bitbucket

