Creating a big fixture
----------------------
python wsgi/django_foodcoop/manage.py dumpdata auth foodcoop > wsgi/fixtures/2013-08-03.json

This could be used to dump data from MySQL production server and load it to sqlite dev DB:
   
	export DJANGO_SETTINGS_MODULE='django_foodcoop.settings.dev'
	python wsgi/django_foodcoop/manage.py loaddata wsgi/fixtures/2013-08-03.json

Creating different fixtures
---------------------------

python wsgi/django_foodcoop/manage.py dumpdata --indent=2 auth.user > wsgi/fixtures/user.json
python wsgi/django_foodcoop/manage.py dumpdata --indent=2 auth.group > wsgi/fixtures/group.json
python wsgi/django_foodcoop/manage.py dumpdata --indent=2 sites.site > wsgi/fixtures/site.json
python wsgi/django_foodcoop/manage.py dumpdata --indent=2 foodcoop.person > wsgi/fixtures/person.json
python wsgi/django_foodcoop/manage.py dumpdata --indent=2 foodcoop.unit > wsgi/fixtures/unit.json
python wsgi/django_foodcoop/manage.py dumpdata --indent=2 foodcoop.sale foodcoop.salestaffmember > wsgi/fixtures/sale.json
python wsgi/django_foodcoop/manage.py dumpdata --indent=2 foodcoop.supplier > wsgi/fixtures/supplier.json
python wsgi/django_foodcoop/manage.py dumpdata --indent=2 foodcoop.product > wsgi/fixtures/product.json
python wsgi/django_foodcoop/manage.py dumpdata --indent=2 foodcoop.saleproduct > wsgi/fixtures/saleproduct.json
python wsgi/django_foodcoop/manage.py dumpdata --indent=2 foodcoop.productorder foodcoop.productfinalorder > wsgi/fixtures/order.json
python wsgi/django_foodcoop/manage.py dumpdata --indent=2 foodcoop.payment foodcoop.chequepayment > wsgi/fixtures/payment.json
python wsgi/django_foodcoop/manage.py dumpdata flatpages --indent=2 > wsgi/fixtures/flatpage.json
python wsgi/django_foodcoop/manage.py dumpdata --indent=2 foodcoop.transfertype foodcoop.transfer > wsgi/fixtures/transfer.json

python wsgi/django_foodcoop/manage.py dumpdata --indent=2 > wsgi/fixtures/all.json
