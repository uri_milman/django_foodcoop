# -*- coding: utf-8 -*-
def fill_delivery_amount(sale):
    for sale_product in SaleProduct.objects.by_sale(sale):
        if sale_product.delivery_amount is None:
            sale_product.delivery_amount = sale_product.amount_received
            sale_product.save()

def create_single(sale, supplier):
    instance, created = PaymentToSupplier.objects.get_or_create(sale=sale, supplier=supplier, defaults={'sum': 0})
    if created:
        instance.sum = SaleProduct.objects.by_sale(sale).by_supplier(supplier).get_delivery_sum()
        instance.save()
        print 'created new payment for sale %d and supplier %d' %(sale.id, supplier.id)
    else:
        print 'skipped creation of payment for sale %d and supplier %d' %(sale.id, supplier.id)

def create_multi(sales, suppliers):
    for sale in sales:
        for supplier in suppliers:
            create_single(sale, supplier)

def main():
    fill_delivery_amount(Sale.objects.filter(pk=1))
    create_multi(sales=Sale.objects.filter(pk=1), suppliers=Supplier.objects.all())
    create_multi(sales=Sale.objects.filter(pk=2), suppliers=Supplier.objects.all())
    create_multi(sales=Sale.objects.filter(pk=3), suppliers=Supplier.objects.all())
