# should be executed from within shell_plus, which will take care of imports
import decimal

from foodcoop.models import VALUE_ADDED_TAX_RATIO, SELLING_PRICE_RATIO

def prices_differ(price1, price2):
    return (price1 > (price2 * decimal.Decimal('1.01')) or \
            price1 < (price2 * decimal.Decimal('0.99')))
    
def validate(sale_id):
    products = Product.objects.order_by('id').all()
    print 'Number of products: %d' %products.count()
    for product in products:
        sale_products = product.saleproduct_set.filter(sale=sale_id)
        if product.deleted:
            #print 'product %d is deleted' %product.id
            for sale_product in sale_products:
                print 'product %d is deleted but has associated sale product %d' %(product.id, sale_product.id)
        else:
            if prices_differ(product.price_before_VAT * VALUE_ADDED_TAX_RATIO, product.price_including_VAT):
                print 'product %d inconsistent VAT prices: %s and %s' %(product.id, product.price_before_VAT, product.price_including_VAT)
            if prices_differ(product.price_including_VAT * SELLING_PRICE_RATIO, product.selling_price):
                print 'product %d inconsistent selling price: %s and %s' %(product.id, product.price_including_VAT, product.selling_price)
            for sale_product in sale_products:
                if prices_differ(sale_product.price, product.selling_price):
                    print 'product %d sale product %d inconsistent prices: %s and %s' \
                        %(product.id, sale_product.id, product.selling_price, sale_product.price)

if __name__ == '__main__':
    validate(sale_id=3)
