suppliers_including_VAT = [3, 17, 18, 19, 20] # bilensky, barel, viler, ornit, avivi  
suppliers_before_VAT = [4, # bibi, 
                        #9, # shimurey - purchase was before change to 18%
                        12, 13, 15, 16] # minhat, michal, zahav, klil     
problematic = [11, # kakao - mixed. maybe an error. waiting for Moran on this
               ]
def print_prices(supplier_ids):
    suppliers = Supplier.objects.filter(id__in=supplier_ids)
    products = Product.objects.filter(supplier__in=supplier_ids).order_by('supplier').values_list('supplier', 'price_before_VAT', 'price_including_VAT')
    for p in products:
        print p

def fix_supplier_prices(supplier_id, fix_price_before_VAT=True):
    products = Product.objects.filter(supplier=supplier_id)
    fix_product_prices(supplier_id, products, fix_price_before_VAT)

def fix_product_prices(supplier_id, products, fix_price_before_VAT=True):
    print 'fixing %d products of supplier %d' %(products.count(), supplier_id)
    for product in products:
        if fix_price_before_VAT:
            product.price_before_VAT = None
        else:
            product.price_including_VAT = None
        product.save()
        
def validate_prices(prices):
    return all([int(p*10) == (p*10) for p in prices])

def check_supplier(supplier_id):
    prices_before = Product.objects.filter(supplier=supplier_id).order_by('supplier').values_list('price_before_VAT', flat=True)
    if validate_prices(prices_before):
        print '%s: need to fix prices including VAT' %supplier_id
        return True
    else:
        prices_including = Product.objects.filter(supplier=supplier_id).order_by('supplier').values_list('price_including_VAT', flat=True)
        if validate_prices(prices_including):
            print '%s: need to fix prices before VAT' %supplier_id
            return True
        else:
            print '%s: Oh oh!!!' %supplier_id
            return False

def initial_fix(): # run once, to fix all supplier prices except for shimurey eikhut
    for s in suppliers_including_VAT:
        fix_supplier_prices(s, fix_price_before_VAT=True)
    
    for s in suppliers_before_VAT:
        fix_supplier_prices(s, fix_price_before_VAT=False)
    
    kakao_products_before_VAT = Product.objects.filter(supplier=11, id__gte=162)
    kakao_products_after_VAT = Product.objects.filter(supplier=11, id__lt=162)
    fix_product_prices(11, kakao_products_before_VAT, False)
    fix_product_prices(11, kakao_products_after_VAT, True)

fix_supplier_prices(9, fix_price_before_VAT=False)
