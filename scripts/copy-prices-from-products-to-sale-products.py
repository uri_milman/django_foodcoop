def copy_product_prices(sale_products):
    for sale_product in sale_products:
        sale_product.price_before_VAT = sale_product.product.price_before_VAT
        sale_product.price_including_VAT = sale_product.product.price_including_VAT
        sale_product.price = sale_product.product.selling_price
        sale_product.save()

def initial_fix(): # copy all SaleProduct prices        
    copy_product_prices(SaleProduct.objects.all())

# copy shimurey eikhut prices with supplier_id=9
def fix_supplier_prices(supplier_id):
    copy_product_prices(SaleProduct.objects.by_supplier(supplier_id))

def fix_sale_prices(sale_id):
    copy_product_prices(SaleProduct.objects.by_sale(sale_id))
