# -*- coding: utf-8 -*-
import pickle
from django.db.models import F

def get_orders():
    return ProductFinalOrder.objects.filter(product_order__product__sale=1)

# save amounts before sale day
def save_amounts(filename):
    amount_values = get_orders().values('id', 'amount')
    amounts = {}
    for v in amount_values:
        amounts[v['id']] = v['amount']
    pickle.dump(amounts, open(filename, 'w'))

def fix_amounts(amounts_before_sale, amounts_after_sale):
    id_list = sorted(amounts_before_sale.keys())
    assert id_list == sorted(amounts_after_sale.keys())
    print 'Working on %d objects' %len(id_list)
    orders = ProductFinalOrder.objects.in_bulk(id_list)
    assert len(orders) == len(id_list)
    diff_ids = []
    for id in id_list:
        orders[id].amount = amounts_before_sale[id]
        orders[id].amount_bought = amounts_after_sale[id]
        orders[id].save()
        if orders[id].amount != orders[id].amount_bought:
            diff_ids.append(id)
    print 'Different amounts in %d orders' %len(diff_ids) 

def doublecheck():
    num_changed = get_orders().exclude(amount=F('amount_bought')).count()
    print 'Different amounts in %d orders' %num_changed
    
def main():
    before_sale_amounts_fn = 'scripts/amounts-before-sale.pkl'
    after_sale_amounts_fn  = 'scripts/amounts-after-sale.pkl' 
    #save_amounts(before_sale_amounts_fn)
    #save_amounts(after_sale_amounts_fn)
    amounts_before_sale = pickle.load(open(before_sale_amounts_fn, 'r'))
    amounts_after_sale  = pickle.load(open(after_sale_amounts_fn, 'r'))
    fix_amounts(amounts_before_sale, amounts_after_sale)

#main()
