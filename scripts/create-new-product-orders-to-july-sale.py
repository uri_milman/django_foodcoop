# -*- coding: utf-8 -*-
sale = Sale.objects.get(pk=2)
sale_product = SaleProduct.objects.get(pk=465) # the round rice which we added after closing the sale...
po1, created = ProductOrder.objects.get_or_create(product=sale_product, member=User.objects.get(pk=10), amount=0)
pfo1, created = ProductFinalOrder.objects.get_or_create(product_order=po1, amount=0)
po2, created = ProductOrder.objects.get_or_create(product=sale_product, member=User.objects.get(pk=22), amount=0)
pfo2, created = ProductFinalOrder.objects.get_or_create(product_order=po2, amount=0)
