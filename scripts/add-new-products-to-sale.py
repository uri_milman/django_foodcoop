# -*- coding: utf-8 -*-
def clear_admin_orders(sale_id):
    print 'Total number of admin orders:', ProductOrder.objects.filter(member__username='admin', product__sale=sale_id).count()
    ProductOrder.objects.filter(member__username='admin', product__sale=sale_id,amount=0).delete()
    print 'Remaining non-zero orders:', ProductOrder.objects.filter(member__username='admin', product__sale=sale_id).count()

def create_new_product_orders(sale_id, product_id, exclude_members=[], create_final_orders=False):
    product = Product.objects.get(pk=product_id)
    sale = Sale.objects.get(pk=sale_id)
    sale_product, created = SaleProduct.objects.get_or_create(product=product, sale=sale,
        defaults=dict(price=product.selling_price, price_before_VAT=product.price_before_VAT,
                      price_including_VAT=product.price_including_VAT, package_size=product.package_size))
    num_orders_created = num_final_orders_created = 0
    for member in User.objects.active_members():
        if member.username in exclude_members:
            continue
        product_order, created = ProductOrder.objects.get_or_create(member=member, product=sale_product, defaults=dict(amount=0))
        if created:
            num_orders_created += 1
        if create_final_orders:
            product_final_order, created = ProductFinalOrder.objects.get_or_create(product_order=product_order, defaults=dict(amount=0))
            if created:
                num_final_orders_created += 1
    print 'Created %d new orders and %d new final orders.' %(num_orders_created, num_final_orders_created)

def august_orders():
    clear_admin_orders(sale_id=3)
    # Relevant products - equivalent products to non-packaged products which are packaged:
    # אורז בשמתי 331 => 181
    # חמוציות 364 => 267
    # עדשים צהובות 297 => 250
    sale_id = 3
    product_ids = (181, 267, 250)
    exclude_members=('rivkapress', 'wahl_99')
    for product_id in product_ids:
        create_new_product_orders(sale_id=sale_id, product_id=product_id, exclude_members=exclude_members)

def sept_orders():
    exclude_members = ('barkanregev', 'wahl_99')
    sale_id = 4
    product_ids = (
        186, # בורגול אורגני
        210, # דוחן אורגני
        188, # קוואקר גס
        249, # עדשים ירוקות
        251, # עדשים שחורות
        261, # שעועית מש
        193, # קינואה לבנה
        260, # שעועית לבנה
        273, # שקדים אורגנים
    )
    for product_id in product_ids:
        create_new_product_orders(sale_id=sale_id, product_id=product_id, exclude_members=exclude_members)

def nov_orders():
    exclude_members = ('wahl_99', 'amy.lipman')
    sale_id = 6
    product_ids = (
        193, # קינואה לבנה
        250, # עדשים צהובות
        251, # עדשים שחורות
        259, # אזוקי
        263, # אגוז ברזיל
        258, # שעועית אדומה
        186, # בורגול אורגני
        209, # חיטה מלאה
        188, # קוואקר גס
        265, # בננה מיובשת
        179, # אורז בשמתי לבן
        177, # אורז עגול מלא
    )
    for product_id in product_ids:
        create_new_product_orders(sale_id=sale_id, product_id=product_id, exclude_members=exclude_members)

def oct_orders():
    exclude_members = ('shani.fabian', 'leora.zamir', 'barkanregev', 'nogaei', 'dafnarz2', 'nir.esterman')
    sale_id = 5
    product_ids = (
        184, # אורז פרא
        269, # צימוקים אורגנים
        189, # קוסקוס אורגני מלא
        187, # קוואקר דק אורגני
        211, # כוסמת ירוקה אורגנית
        262, # שעועית שחורה אורגנית
        191, # קינואה אדומה אורגנית
    )
    for product_id in product_ids:
        create_new_product_orders(sale_id=sale_id, product_id=product_id, exclude_members=exclude_members)

def dec_orders():
    '''add products to Yosi Avivi instead of products from Hertzl Bibi'''
    exclude_members = ('nogaei', 'lee4444')
    sale_id = 7
    product_ids = (
        310, # gar'iney dla'at
        309, # gar'iney hamania
        24, # kvaker gas
        25, # kvaker tahun
    )
    for product_id in product_ids:
        create_new_product_orders(sale_id=sale_id, product_id=product_id, exclude_members=exclude_members, create_final_orders=True)

def dec_orders_2():
    '''add products to Hertzl Bibi due to last changes in actual order'''
    exclude_members = ('nogaei', 'lee4444')
    sale_id = 7
    product_ids = (
        198, # kusmin spaghetti
    )
    for product_id in product_ids:
        create_new_product_orders(sale_id=sale_id, product_id=product_id, exclude_members=exclude_members, create_final_orders=True)
