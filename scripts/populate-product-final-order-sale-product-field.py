product_final_orders = ProductFinalOrder.objects.all()
print "Updating %d ProductFinalOrder items..." %(len(product_final_orders))

for product_final_order in product_final_orders:
    product_final_order.sale_product = product_final_order.product_order.product
    product_final_order.save()

print 'Done'
