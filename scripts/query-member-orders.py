# should be executed from within shell_plus, which will take care of imports
from django.db.models import Count

import decimal

#def get_problematic_orders_by_amount(sale_id):
#    print 'Orders with non-integer amount:'
#    pos = ProductOrder.objects.by_sale(sale_id).exclude(amount__in=[decimal.Decimal(i) for i in range(0,20)])
#    for p in pos:
#        print p.info()
#    print ''

def get_orders_by_member(sale_id):
    print 'Member orders ordered by number of products:'
    results = ProductOrder.objects.by_sale(sale_id).filter(amount__gt=0).values('member__username').annotate(num_products=Count('member__username')).order_by('num_products')
    for r in results:
        print r
    print 'Total product orders: %d' %(sum([r['num_products'] for r in results]))
    print ''
    
def get_members_without_orders(sale_id):
    print 'Members without any orders:'
    ordering_member_ids = ProductOrder.objects.by_sale(sale_id).filter(amount__gt=0).values_list('member__id', flat=True).distinct().order_by('member__id')
    for member in User.objects.exclude(id__in=ordering_member_ids):
        print 'id %s username %s' %(member.id, member.username)
    print ''
 
def query(sale_id):
    get_orders_by_member(sale_id)
    get_members_without_orders(sale_id)
#    get_problematic_orders_by_amount(sale_id)

#if __name__ == '__main__':
#    query(sale_id=2)
