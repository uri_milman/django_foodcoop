#!/usr/bin/env python

from setuptools import setup

setup(
    name='django_foodcoop',
    version='1.0',
    description='OpenShift App',
    author='Ophir Tal',
    author_email='firtzel@gmail.com',
    url='',
    install_requires=[
        'Django==1.5.1',
        'pinax-theme-bootstrap==3.0a2',
        'pinax-theme-bootstrap-account==1.0b2',
        'django-user-accounts==1.0b8',
        'django-forms-bootstrap==2.0.3.post1',
#        'metron==1.1',
#        'pinax-utils==1.0b1.dev3',
        'eventlog==0.6',
        'django-jsonfield==0.8.12',
        'South==0.7.6',
        'django-debug-toolbar==0.9.4',
        'django-extensions==1.1.1',
        'ipython==0.13.2',
        'django-extra-views==0.6.2',
    ],
    dependency_links=[
        "http://dist.pinaxproject.com/dev/",
        "http://dist.pinaxproject.com/alpha/",
    ],
)
