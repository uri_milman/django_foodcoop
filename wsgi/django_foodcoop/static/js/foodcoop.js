// A small hack to convert the float amount values sent by the back-end to integer values whenever possible 
function hack_quantity_field_value(index, elem) {
	if (jQ(elem).is("input")) {
		float_value = parseFloat(jQ(elem).val());
		int_value = parseInt(jQ(elem).val());
		if (float_value == int_value) {
			jQ(elem).val(int_value);
		}
	} else {
		float_value = parseFloat(jQ(elem).html());
		int_value = parseInt(jQ(elem).html());
		if (float_value == int_value) {
			jQ(elem).html(int_value);
		}
	}
}

function truncate_decimal_field(index, elem) {
	if (jQ(elem).is("input")) {
		float_value = parseFloat(jQ(elem).val());
		jQ(elem).val(float_value.toFixed(2));
	} else {
		float_value = parseFloat(jQ(elem).html());
		jQ(elem).html(float_value.toFixed(2));
	}
}

function updateMessage(message) {
	var message_elem = jQ('ul.messagelist');
	message_elem.children().remove();
	message_elem.append("<li class='info'>" + message + "</li>");
}

function sumElements(elements, sum_elem) {
	var total = 0;
	jQ.each(elements, function(index, elem) {
		total = total + parseFloat(jQ(elem).html());
	});
	sum_elem.html(total.toFixed(2));
}
