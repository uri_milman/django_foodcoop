from django.conf.urls import patterns, include, url
from django.views.decorators.cache import never_cache
from django.views.generic import TemplateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r"^$", never_cache(TemplateView.as_view(template_name="homepage.html")), name="home"),

    url(r"^account/", include("account.urls")),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^foodcoop/', include('foodcoop.urls')),

    url(r'^tinymce/', include('tinymce.urls')),

    url(r'^upcoming/$', 'django.contrib.flatpages.views.flatpage', {'url': '/upcoming/'}, name='upcoming'),
    url(r'^announcesale/$', 'django.contrib.flatpages.views.flatpage', {'url': '/announcesale/'}, name='announcesale'),
#    url(r'^cache-manifest/$', 'django_foodcoop.views.cache_manifest', name='cache-manifest'),
#    url(r'^offline/$', TemplateView.as_view(template_name="offline.html"), name='offline-fallback'),
#    url(r'^cache-manifest/$', CacheManifestView.as_view(), name='cache-manifest'),
)
