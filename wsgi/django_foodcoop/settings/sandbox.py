import os

from defaults import *

ENVIRONMENT = SANDBOX_ENV
ON_OPENSHIFT = True
DEBUG = True
DEBUG_TOOLBAR_CONFIG['SHOW_TOOLBAR_CALLBACK'] = custom_show_toolbar

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'coopsandbox',
        'USER': os.environ['OPENSHIFT_MYSQL_DB_USERNAME'],
        'PASSWORD': os.environ['OPENSHIFT_MYSQL_DB_PASSWORD'],
        'HOST': os.environ['OPENSHIFT_MYSQL_DB_HOST'],
        'PORT': os.environ['OPENSHIFT_MYSQL_DB_PORT'],
    }
}

SITE_ID = 2

update_openshift_secret_key()
