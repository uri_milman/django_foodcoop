import os

from defaults import *

ENVIRONMENT = PRODUCTION_ENV
ON_OPENSHIFT = True
DEBUG = True # TODO: once the application is well baked, change to False
DEBUG_TOOLBAR_CONFIG['SHOW_TOOLBAR_CALLBACK'] = custom_show_toolbar

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'foodcoop',
        'USER': os.environ['OPENSHIFT_MYSQL_DB_USERNAME'],
        'PASSWORD': os.environ['OPENSHIFT_MYSQL_DB_PASSWORD'],
        'HOST': os.environ['OPENSHIFT_MYSQL_DB_HOST'],
        'PORT': os.environ['OPENSHIFT_MYSQL_DB_PORT'],
        'OPTIONS': {
            'init_command': 'SET storage_engine=MyISAM,character_set_connection=utf8,collation_connection=utf8_general_ci',
        },
    }
}

SITE_ID = 3

update_openshift_secret_key()
