import sys

from dev import *

if 'test' not in sys.argv:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'foodcoop',
            'USER': 'foodcoop',
            'PASSWORD': 'foodcoop',
            'HOST': 'localhost',
            'PORT': 3306,
#            'DEFAULT_STORAGE_ENGINE': 'MyISAM',
            'OPTIONS': {
                'init_command': 'SET storage_engine=MyISAM,character_set_connection=utf8,collation_connection=utf8_general_ci',
            },
        }
    }
