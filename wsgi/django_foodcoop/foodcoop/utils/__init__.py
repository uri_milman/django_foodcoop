FIXTURES = ('user.json', 'userprofile.json', 'person.json', 'sale.json', 'site.json', 'supplier.json',
            'unit.json', 'product.json', 'saleproduct.json', 'group.json', 'order.json',
            'payment.json', 'flatpage.json', 'transfer.json')

TEST_FIXTURES = FIXTURES
