from django import forms
from django.contrib.auth.models import User
from django.contrib import admin
from django.contrib.admin.helpers import Fieldset
from django.contrib.auth.admin import UserAdmin
from django.contrib.flatpages.admin import FlatpageForm, FlatPageAdmin
from django.contrib.flatpages.models import FlatPage

from tinymce.widgets import TinyMCE

from foodcoop.models import *

class ExtendedFlatPageForm(FlatpageForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 160, 'rows': 30}))
    
class ExtendedFlatPageAdmin(FlatPageAdmin):
    form = ExtendedFlatPageForm

class SaleProductInline(admin.TabularInline):
    model = SaleProduct
    fieldsets = [
        (None, {'fields': ['product', 'sale', 'price', 'package_size']}),
    ]
    extra = 3

class SaleStaffMemberInline(admin.TabularInline):
    model = SaleStaffMember
    extra = 3
    
class SaleAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'sale_date', 'status', 'closing_date')
    fieldsets = [
        (None, {'fields': ['sale_date', 'title', 'closing_date', 'status']}),
    ]
    # TODO: show products in sale page - currently triggers a lot of SQL queries which break stuff 
    inlines = [SaleStaffMemberInline] #SaleProductInline]

class PersonInline(admin.TabularInline):
    model = Person

class UserProfileInline(admin.StackedInline):
    model = UserProfile

class FoodCoopUserAdmin(UserAdmin):
    inlines = (UserProfileInline, PersonInline)

class TransferTypeAdmin(admin.ModelAdmin):
    list_display = ('description', 'expense')

class TransferAdmin(admin.ModelAdmin):
    list_display = ('type', 'user', 'sale', 'transfer_date', 'sum', 'description', 'supplier')
    list_filter = ('sale', 'type')

class ProductInline(admin.TabularInline):
    model = Product
    fieldsets = [
        (None, {'fields': ['name', 'unit', 'category', 'price_including_VAT', 'packaged', 'organic', 'description', 'package_size', 'deleted']}),
    ]
    extra = 3
    
class SupplierAdmin(admin.ModelAdmin):
    list_display = ('name', )
    inlines = [ProductInline]

class InventoryFilter(admin.SimpleListFilter):
    title = 'inventory'
    parameter_name = 'inventory'
    
    def lookups(self, request, model_admin):
        return (
            ('non-zero', 'In Stock'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'non-zero':
            return queryset.filter(inventory__gt = 0)



class TariffAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_display = ('id', 'type', 'factor')
    
class ProductAdmin(admin.ModelAdmin):
    list_filter = (InventoryFilter, 'supplier', 'category', 'deleted')
    search_fields = ('name',)
    list_display = ('id', 'name', 'supplier', 'category', 'unit', 'inventory', 'price_before_VAT', 'price_including_VAT', 'packaged', 'organic', 'package_size', 'description', 'deleted')

class SaleProductAdmin(admin.ModelAdmin):
    search_fields = ('product__name',)
    list_filter = ('sale', 'product__supplier', 'product__category')
    list_display = ('id', 'product', 'sale', 'price_before_VAT', 'price_including_VAT', 'package_size',
                    #'amount_ordered',
                    'amount_received', 'pre_sale_comments', 'post_sale_comments')

class NonZeroAmountFilter(admin.SimpleListFilter):
    title = 'amount'
    parameter_name = 'amount'
    
    def lookups(self, request, model_admin):
        return (
            ('non-zero', 'Non zero'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'non-zero':
            return queryset.non_zero()

class ProductFinalOrderAmountFilter(admin.SimpleListFilter):
    title = 'amount'
    parameter_name = 'amount'
    
    def lookups(self, request, model_admin):
        return (
            ('non-zero', 'Non zero'),
            ('bought-diff', 'Amount bought differs from amount'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'non-zero':
            return queryset.non_zero()
        elif self.value() == 'bought-diff':
            return queryset.amount_differs()

class ProductOrderAdmin(admin.ModelAdmin):
    search_fields = ('product__product__name',)
    list_filter = ('product__sale', 'product__product__supplier', 'member', NonZeroAmountFilter)
    list_display = ('id', 'member_full_name', 'product', 'supplier', 'amount', 'comments')

    readonly_fields=('member_full_name', 'product')

    def queryset(self, request):
        return super(ProductOrderAdmin, self) \
            .queryset(request) \
            .select_related('product', 'product__product', 'member', 'product__product__supplier')

class ProductFinalOrderAdmin(admin.ModelAdmin):
    search_fields = ('product_order__product__product__name',)
    list_filter = ('product_order__product__sale', 'product_order__product__product__supplier',
                   'product_order__member', ProductFinalOrderAmountFilter)
    list_display = ('product_name', 'supplier', 'member_full_name', 'amount', 'amount_bought')

    readonly_fields = ('product_order',)

    def queryset(self, request):
        return super(ProductFinalOrderAdmin, self).queryset(request).select_related()

class PaymentAdmin(admin.ModelAdmin):
    list_display = ('sale', 'member', 'sum', 'took_order', 'sum_paid', 'method', 'updated_by', 'comments')
    list_filter = ('member', 'sale', 'method')

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, ExtendedFlatPageAdmin)
admin.site.unregister(User)
admin.site.register(User, FoodCoopUserAdmin)

admin.site.register(Sale, SaleAdmin)
admin.site.register(Tariff, TariffAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(SaleProduct, SaleProductAdmin)
admin.site.register(ProductOrder, ProductOrderAdmin)
admin.site.register(ProductFinalOrder, ProductFinalOrderAdmin)
admin.site.register(Supplier, SupplierAdmin)
admin.site.register(SupplierCategory, list_display=('id', 'name', 'supplier'), ordering = ('name',))
admin.site.register(Unit, list_display=('id', 'unit'))
admin.site.register(Person)
admin.site.register(Transfer, TransferAdmin)
admin.site.register(TransferType, list_display=('description', 'expense', 'supplier', 'user', 'comments'))
admin.site.register(Payment, PaymentAdmin)
admin.site.register(PaymentToSupplier, list_display=('sale', 'supplier', 'sum', 'comments'), list_filter=('sale', 'supplier'))
admin.site.register(ChequePayment, list_display=('payment', 'cheque_number', 'account_number', 'account_name', 'envelope_internal_id', 'bank'))
admin.site.register(OtherPayment, list_display=('id', 'sum', 'expense', 'date', 'description', 'comments'),
                    search_fields=('description', 'comments'), list_filter=('expense',))
