import decimal

from django.contrib.auth.models import User

from foodcoop.models import *

from base import BaseTestCase

class SaleTest(BaseTestCase):
    def test_latest_open_to_buyers(self):
        sale = Sale.objects.latest_open_to_buyers()
        self.assertNotEqual(sale, None)
        self.assertEquals(sale.id, 2)

    def test_group_by(self):
        sale_products = SaleProduct.objects.by_sale(Sale.objects.get(pk=1)).group_by_product_id()
        self.assertEquals(sale_products.keys(), [1, 2, 4, 5, 6, 7, 8, 9])

class SupplierTest(BaseTestCase):
    def test_user_managed_suppliers(self):
        user = User.objects.get(pk=1)
        suppliers = user.supplier_set.all()
        self.assertEquals(len(suppliers), 0)
        user = User.objects.get(pk=2)
        suppliers = user.supplier_set.all()
        self.assertEquals(len(suppliers), 2)

class ProductTest(BaseTestCase):
    def test_get_all_products(self):
        self.assertSortedIdsEqual(Product.objects.filter(supplier__id=1).all(), [1,2,3,4,5])
    
    def test_create_product(self):
        product = Product(name='test-product', unit=Unit.objects.get(pk=1), supplier=Supplier.objects.get(pk=1),
                          packaged=0, organic=0, package_size=decimal.Decimal('1.0'),
                          price_before_VAT=decimal.Decimal('10.0'))
        product.save()
        self.assertEquals(product.price_before_VAT, decimal.Decimal('10.0'))
        self.assertEquals(product.price_including_VAT, decimal.Decimal('11.8'))
        self.assertEquals(product.selling_price, decimal.Decimal('12.39'))
#    def test_get_non_deleted_products(self):
#        self.assertIdsEqual(Product.objects.filter(supplier__id=1, deleted=False).all(), [1,2,4,5])

class OrderTest(BaseTestCase):
    def test_some_query(self):
        sale = Sale.objects.get(pk=1)
        supplier = Sale.objects.get(pk=1)
        responsible = User.objects.get(pk=2)
        products = sale.saleproduct_set.select_related().all()
        self.assertIdsEqual(products, range(1, 9))
        products = [p for p in products if responsible in p.product.supplier.buyers.all()]
        self.assertIdsEqual(products, range(1, 5))

class ProductOrderTest(BaseTestCase):
    def test_get_orders_for_sale(self):
        sale = Sale.objects.get(pk=3)
        member = User.objects.get(pk=2)
        orders = ProductOrder.objects.by_sale(sale).by_member(member).non_zero().order_by('id')
        self.assertIdsEqual(orders, [29,30,31,34])

    def test_get_final_orders_for_sale(self):
        sale = Sale.objects.get(pk=3)
        member = User.objects.get(pk=2)
        orders = ProductFinalOrder.objects.by_sale(sale).by_member(member).non_zero()
        self.assertIdsEqual(orders, [17,19,20,21])

    def test_get_member_orders_info(self):
        sale = Sale.objects.get(pk=3)
        supplier = Supplier.objects.get(pk=1)
        info = ProductOrder.objects.get_member_orders_info(sale=3, supplier=2)
        self.assertSortedIdsEqual(info['products'], [16, 17, 18])
        self.assertSortedIdsEqual(info['members'], [2, 3, 4])
        # TODO: test values in info['member_orders']

class SaleProductTest(BaseTestCase):
    def test_amount_bought(self):
        sale_product = SaleProduct.objects.get(pk=1)
        self.assertEquals(sale_product.amount_bought(), decimal.Decimal('18'))

#class PaymentTest(BaseTestCase):
#    def test_get_payments(self):
#        sale = Sale.objects.get(pk=3)

class FinalizeSaleTest(BaseTestCase):
    def test_finalize(self):
        pass
        # TODO: finalize order, see that saleproduct items are being created

class UserTest(BaseTestCase):
    def test_contact_details(self):
        user = User.objects.get(pk=2)
        info = user.get_profile().contact_details()
        self.assertEquals(info, ['Billy Elliot: 050-1212123 / 08-1212123',
                                 'Billy The Kid: 050-1212124 / 08-1212124'])
