from django.test import TestCase

from foodcoop.utils import TEST_FIXTURES

class BaseTestCase(TestCase):
    fixtures = TEST_FIXTURES
    
    def assertIdsEqual(self, objects, expected_ids):
        self.assertEquals([o.id for o in objects], expected_ids)

    def assertSortedIdsEqual(self, objects, expected_ids):
        self.assertEquals(sorted([o.id for o in objects]), expected_ids)
    
    def login(self, username):
        self.assertTrue(self.client.login(username=username, password=username))
    
    def logout(self):
        self.client.logout()
