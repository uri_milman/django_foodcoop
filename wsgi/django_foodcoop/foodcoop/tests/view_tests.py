# -*- coding: utf-8 -*-
from decimal import Decimal

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from foodcoop.models import *
from foodcoop.views import *

from base import BaseTestCase

class BasicViewsTest(BaseTestCase):
    def view_test(self, url, username):
        response = self.client.get(url)
        self.assertEquals(response.status_code, 302)
        self.login(username)
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)
        
    def test_member_list(self):
        self.view_test(reverse('member-list'), 'customer1')
        
    def test_member_order_form(self):
        self.view_test(reverse('member-order-form', kwargs={'sale_id': 4}), 'customer1')
        
    def test_member_order_list(self):
        self.view_test(reverse('member-order-list', kwargs={'pk': 4}), 'customer1')
        
    def test_supplier_products(self):
        self.view_test(reverse('supplier-products', kwargs={'pk': 1}), 'Bob')
        
    def test_edit_supplier_products(self):
        self.view_test(reverse('edit-supplier-products', kwargs={'pk': 2}), 'Billy')
        
    def test_transfer_list(self):
        self.view_test(reverse('transfer-list', kwargs={}), 'tracy')
        
    def test_payments_breakdown(self):
        self.view_test(reverse('payments-breakdown', kwargs={}), 'tracy')
        
    def test_payments_by_member(self):
        self.view_test(reverse('payments-by-member', kwargs={'member_id': 2}), 'tracy')
        
    def test_payments_by_sale(self):
        self.view_test(reverse('payments-by-sale', kwargs={'sale_id': 3}), 'tracy')
        
    def test_manage_sale(self):
        self.view_test(reverse('manage-sale', kwargs={'sale_id': 3}), 'sally')

    def test_manage_cheques(self):
        self.view_test(reverse('manage-cheques', kwargs={'pk': 3}), 'sally')

    def test_member_orders_by_sale(self):
        self.view_test(reverse('member-orders-by-sale', kwargs={'sale_id': 3, 'member_id': 3}), 'sally')

class UpdateSaleTest(BaseTestCase):
    def update_and_assert(self, data, product_ids, num_added, num_deleted):
        response = self.client.get('/foodcoop/supplier/1/update/', data)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "%d מוצרים נוספו, %d מוצרים הוסרו" %(num_added, num_deleted))
        self.assertSortedIdsEqual(SaleProduct.objects.filter(sale__exact=Sale.objects.get(pk=1)), product_ids)

    # TODO: fix test
#    def test_update_sale_products(self):
#        data = {
#            'sale_id': '1',
#            'selection': '{"1":false,"2":false,"3":false}',
#        }
#        self.update_and_assert(data, [3,4,5,6,7,8], 0, 2)
#        data.update({'selection': '{"1":true,"2":true,"3":false}'})
#        self.update_and_assert(data, [1,2], 2, 0)
#        data.update({'selection': '{"1":false,"2":false,"3":true}'})
#        self.update_and_assert(data, [3], 1, 2)
#        data.update({'selection': '{"1":false,"2":false,"3":false}'})
#        self.update_and_assert(data, [], 0, 1)

class ManageOrderTest(BaseTestCase):
    def test_something(self):
        sale = Sale.objects.get(pk=1)
        self.assertEquals(sale.is_draft(), True)

class MemberOrdersViewTest(BaseTestCase):
    def setUp(self):
        self.login('sally')
        self.sale = Sale.objects.get(pk=3)
        self.member = User.objects.get(pk=2)
        self.url = reverse('member-orders-by-sale', kwargs={'sale_id': self.sale.id, 'member_id': self.member.id})

    cash_data = {
        'comments': u'there you have it', 'sum_paid': Decimal('123'), 'took_order': True, 'method': u'cash',
    }
    invalid_data = cash_data.copy()
    del invalid_data['sum_paid']

    invalid_cheque_data = {
        'comments': u'bank it', 'sum_paid': Decimal('480'), 'took_order': True, 'method': u'cheque',
        'account_number': '1', 'account_name': 'salma', 'envelope_internal_id': 1, 'bank': 12,
    }
    cheque_data = {
        'comments': u'bank it', 'sum_paid': Decimal('480'), 'took_order': True, 'method': u'cheque',
        'cheque_number': '33/156', 'account_number': '2055', 'account_name': 'salma', 'envelope_internal_id': 1, 'bank': 12,
    }

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEquals(response.status_code, 200)
    
    def test_invalid_data(self):
        response = self.client.post(self.url, self.invalid_data)
        self.assertFormError(response, 'payment_form', 'sum_paid', [])
    
    def test_cash_payment(self):
        response = self.client.post(self.url, self.cash_data)
        self.assertRedirects(response, reverse('manage-sale', args=(3,)))
        payment = Payment.objects.get(sale=self.sale, member=self.member)        
        self.assertEquals(payment.took_order, True)
        self.assertEquals(payment.sum_paid, Decimal('123'))
        self.assertEquals(payment.method, 'cash')
        self.assertEquals(payment.comments, 'there you have it')
        self.assertEquals(payment.updated_by.id, 6)
    
    def test_invalid_cheque_data(self):
        response = self.client.post(self.url, self.invalid_cheque_data)
        self.assertFormError(response, 'chequepayment_form', 'cheque_number', [])
        
    def test_cheque_payment(self):
        response = self.client.post(self.url, self.cheque_data)
        self.assertRedirects(response, reverse('manage-sale', args=(3,)))
        payment = Payment.objects.get(sale=self.sale, member=self.member)
        self.assertEquals(payment.took_order, True)
        self.assertEquals(payment.sum_paid, Decimal('480'))
        self.assertEquals(payment.method, 'cheque')
        self.assertEquals(payment.comments, 'bank it')
        self.assertEquals(payment.updated_by.id, 6)
        cheque_payment = ChequePayment.objects.get(payment=payment)
        self.assertEquals(cheque_payment.cheque_number, '33/156')
        self.assertEquals(cheque_payment.account_number, '2055')
        self.assertEquals(cheque_payment.account_name, 'salma')
        self.assertEquals(cheque_payment.envelope_internal_id, 1)
        self.assertEquals(cheque_payment.bank, 12)
        
class MemberOrderFormTest(BaseTestCase):
    def setUp(self):
        self.sale = Sale.objects.get(pk=4)
        self.member = User.objects.get(pk=4)
        self.url = reverse('member-order-form', kwargs={'sale_id': self.sale.id})
        self.login(self.member.username)
    
    def get_orders(self):
        return ProductOrder.objects.filter(member=self.member, product__sale=self.sale)
    
    def get_num_orders(self):
        return self.get_orders().count()
    
    def test_display_form(self):
        self.assertEquals(self.get_num_orders(), 0, 'initial number of product orders should be zero')
        response = self.client.get(self.url)
        self.assertEquals(self.get_num_orders(), 3, 'after first load of the page, initial orders should be created and saved')
        response = self.client.get(self.url)
        self.assertEquals(self.get_num_orders(), 3, 'subsequent calls should not create new orders')
    
    valid_data = {
        'form-0-id': 36,
        'form-0-amount': 2,
        'form-0-min_amount': 2,
        'form-0-max_amount': 3,
        'form-0-amount': 2,
        'form-1-id': 37,
        'form-1-amount': 5,
        'form-1-min_amount': 4,
        'form-1-max_amount': 6,
        'form-2-id': 38,
        'form-2-amount': 0,
        'form-2-min_amount': 0,
        'form-2-max_amount': 0,
        'form-TOTAL_FORMS': 3,
        'form-INITIAL_FORMS': 3,
        'form-MAX_NUM_FORMS': 1000,              
    }
    expected_product_orders = [{'product_id': 22, 'max_amount': Decimal('3'), 'min_amount': Decimal('2'), 'comments': u'', 'amount': Decimal('2'), 'member_id': 4, u'id': 36},
                               {'product_id': 23, 'max_amount': Decimal('6'), 'min_amount': Decimal('4'), 'comments': u'', 'amount': Decimal('5'), 'member_id': 4, u'id': 37},
                               {'product_id': 24, 'max_amount': Decimal('0'), 'min_amount': Decimal('0'), 'comments': u'', 'amount': Decimal('0'), 'member_id': 4, u'id': 38}]
    def test_submit_form(self):
        response = self.client.post(self.url, self.valid_data)
        self.assertRedirects(response, self.url)
        product_orders = self.get_orders()
        results = product_orders.values()
        self.assertEquals(len(results), len(self.expected_product_orders), 'unexpected number of results')
        for i in range(len(self.expected_product_orders)):
            self.assertEquals(results[i], self.expected_product_orders[i], 'unexpected result %d' %i)
        #self.assertEquals(product_orders.values(), self.expected_product_orders) this doesn't work for some reason

    #def test_multiple_user_orders(self):
    # TODO: implement this sometime soon

class MakeDraftSaleTest(BaseTestCase):
    def setUp(self):
        self.sale = Sale.objects.get(pk=4)
        self.url = reverse('draft-sale', kwargs={'sale_id': self.sale.id})
    
    def test_draft_sale(self):
        product_final_orders = ProductFinalOrder.objects.filter(product_order__product__sale=self.sale)
        self.assertEquals(product_final_orders.count(), 0)
        self.login('admin')
        response = self.client.get(self.url)
        self.logout()
        self.assertEquals(response.status_code, 200)
        self.assertEquals(Sale.objects.get(pk=self.sale.id).status, 'draft')
        product_final_orders = ProductFinalOrder.objects.filter(product_order__product__sale=self.sale)
        self.assertEquals(product_final_orders.count(), 1)
        self.login('Bob')
        response = self.client.get('/foodcoop/sale/4/supplier/1/')
        self.assertEquals(response.status_code, 200)

class FinalizeSaleTest(BaseTestCase):
    def setUp(self):
        self.sale = Sale.objects.get(pk=1)
        self.url = reverse('finalize-sale', kwargs={'sale_id': self.sale.id})
    
    def test_finalize_sale(self):
        response = self.client.get(self.url)
        self.assertEquals(response.status_code, 200)
        changed_sale = Sale.objects.get(pk=self.sale.id)
        self.assertEquals(changed_sale.status, 'final')
