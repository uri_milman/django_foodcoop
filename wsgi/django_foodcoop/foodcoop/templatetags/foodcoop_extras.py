from django import template

register = template.Library()

@register.filter
def get_item(dictionary, key):
    if dictionary is None or type(dictionary) is not dict:
        return 0
    return dictionary.get(key, 0)

@register.filter
def get_item_or_empty_dict(dictionary, key): # TODO: this is a hack. remove it
    if dictionary is None or type(dictionary) is not dict:
        return {}
    return dictionary.get(key, {})

@register.filter    
def subtract_inventory(value, arg):
    ans = value - arg
    if ans > 0:
        return ans
    else:
        return 0

@register.filter    
def multiply(value, arg):
    try:
       return "%.2f" % (value*arg)  
    except Error:
       return -1
  

