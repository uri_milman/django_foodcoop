# -*- coding: utf-8 -*-
import decimal

from django.db import models
from django.db.models import F, Q
from django.db.models.query import QuerySet
from django.db.models.signals import pre_save, post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
 
VALUE_ADDED_TAX_RATIO = decimal.Decimal('1.18')
SELLING_PRICE_RATIO = decimal.Decimal('1.05')

__all__ = (
    'Sale', 'UserProfile', 'ProductOrder', 'ProductFinalOrder', 'Product',
    'Transfer', 'Payment', 'Supplier', 'SaleProduct', 'ChequePayment',
    'Unit', 'ChequeEnvelope', 'SaleStaffMember', 'Person', 'TransferType',
    'SupplierCategory', 'PaymentToSupplier', 'OtherPayment', 'Tariff',
    'VALUE_ADDED_TAX_RATIO', 'SELLING_PRICE_RATIO',
    'METHOD_CASH', 'METHOD_CHEQUE', 'METHOD_BANK_TRANSFER',
)

class Tariff(models.Model):
    '''
    Tariff type
    '''
    type = models.CharField(max_length=25)
    factor = models.DecimalField(max_digits=10, decimal_places=2)

    def __unicode__(self):
        return self.type

class CustomQuerySetManager(models.Manager):
    """A re-usable Manager to access a custom QuerySet - taken from http://stackoverflow.com/a/2163921"""
    def __getattr__(self, attr, *args):
        try:
            return getattr(self.__class__, attr, *args)
        except AttributeError:
            return getattr(self.get_query_set(), attr, *args)

    def get_query_set(self):
        return self.model.QuerySet(self.model)

def decimal_field(max_digits=10, decimal_places=2, **kwargs):
    return models.DecimalField(max_digits=max_digits, decimal_places=decimal_places, **kwargs)

class UserProfile(models.Model):  
    DEFAULT_SHARE_PRICE = 300
    user = models.ForeignKey(User, unique=True)
    user_number = models.IntegerField(unique=True, null=True, blank=True)
    joined_at = models.DateField(null=True, blank=True)
    left_at = models.DateField(null=True, blank=True)
    bought_share_at = models.DateField(null=True, blank=True)
    share_price = decimal_field(default=DEFAULT_SHARE_PRICE, null=True, blank=True)
    tariff = models.ForeignKey(Tariff)

    def __unicode__(self):
        return unicode(self.user)

    def contact_details(self):
        info = []
        for person in self.user.persons.all():
            person_info = ' / '.join([str(d) for d in [person.cellular_phone_number, person.home_phone_number] if d])
            if person_info:
                info.append('%s: %s' %(person.name, person_info))
        return info

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    """Create a matching profile whenever a User is created."""
    if created and not kwargs.get('raw', False):
        UserProfile.objects.get_or_create(user=instance)

class Person(models.Model):
    '''
    A user represents a household which is a member of the cooperative.
    Person links to the individuals, who may have different names, phone numbers, etc.
    '''   
    user = models.ForeignKey(User, related_name='persons')
    name = models.CharField(max_length=50, unique=True)
    home_phone_number = models.CharField(max_length=20, blank=True, null=False)
    cellular_phone_number = models.CharField(max_length=20, blank=True, null=False)
    email_address = models.EmailField(blank=True, null=False)

    def __unicode__(self):
        return self.name

class SaleManager(models.Manager):
    def latest_by_status(self, status):
        '''
        Get the latest sale according to a given status (open to buyers, open to all, etc.) if exists.
        Otherwise return None.
        '''
        sales = self.filter(status__exact=status).order_by("-sale_date")[:1]
        return None if len(sales) == 0 else sales[0]

    def latest_open_to_buyers(self):
        return self.latest_by_status(Sale.OPEN_TO_BUYERS)

    def latest_open_to_all(self):
        return self.latest_by_status(Sale.OPEN_TO_ALL)

    def latest_draft(self):
        return self.latest_by_status(Sale.DRAFT)

    def latest_ready(self):
        return self.latest_by_status(Sale.FINAL)

    def archived(self):
        return self.filter(status__in=[Sale.FINAL, Sale.CLOSED]).order_by('-sale_date')

class Sale(models.Model):
    '''
    Sale life cycle:
    1) created by admin as OPEN_TO_BUYERS and informs buyers
    2) all buyers edit their suppliers product list, and inform admin
    3) admin changes status to OPEN_TO_ALL and informs members
    4) all members edit their orders
    5) admin changes status to DRAFT and informs buyers
    6) (TODO: needs re-thinking) manually fix orders to comply with restrictions, such as package sizes
    7) sale is ready - admin changes status to FINAL
    8) after sale is really over, admin changes to CLOSED
    '''
    OPEN_TO_BUYERS = 'open_to_buyers'
    OPEN_TO_ALL    = 'open_to_all'
    DRAFT          = 'draft'
    FINAL          = 'final'
    CLOSED         = 'closed'
    STATUS_CHOICES = [(x,x) for x in (OPEN_TO_BUYERS, OPEN_TO_ALL, DRAFT, FINAL, CLOSED)]
    
    title = models.CharField(max_length=50)
    sale_date = models.DateTimeField()
    closing_date = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=15, default=OPEN_TO_BUYERS, choices=STATUS_CHOICES)
    comments = models.TextField(verbose_name='הערות')

    objects = SaleManager()

    def is_open_to_buyers(self):
        return (self.status == Sale.OPEN_TO_BUYERS)
    
    def is_open(self):
        return (self.status == Sale.OPEN_TO_ALL)

    def is_draft(self):
        return (self.status == Sale.DRAFT)
    
    def is_finalized(self):
        return (self.status == Sale.FINAL)

    def is_closed(self):
        return (self.status == Sale.CLOSED)

    def __unicode__(self):
        return self.title

    def get_product_statistics(self):
        categories = []
        for supplier in Supplier.objects.select_related().all():
            if supplier.has_categories():
                for supplier_category in supplier.suppliercategory_set.all():
                    categories.append({
                        'name': supplier_category.name,
                        'num_products': self.saleproduct_set.filter(product__category=supplier_category).count(),
                    })
            else:
                categories.append({
                    'name': supplier.name,
                    'num_products': self.saleproduct_set.filter(product__supplier=supplier).count(),
                })
        return categories
        
class SaleStaffMember(models.Model):
    MANAGER = 'manager'
    # TODO: rename HELPER, maybe rename REGISTER
    REGISTER = 'register'
    HELPER = 'helper'
    
    ROLE_CHOICES = (
        (MANAGER, 'sale day manager'),
        (REGISTER, 'working the register'),
        (HELPER, 'helping everyone'),
    )
    
    sale = models.ForeignKey(Sale, related_name='staff_members')
    member = models.ForeignKey(User)
    role = models.CharField(max_length=20, choices=ROLE_CHOICES)

class Supplier(models.Model):
    name = models.CharField(max_length=100)
    buyers = models.ManyToManyField(User, blank=True)

    def has_categories(self):
        return (self.suppliercategory_set.count() > 0)

    def __unicode__(self):
        return self.name

class SupplierCategory(models.Model):
    name = models.CharField(max_length=100)
    supplier = models.ForeignKey(Supplier, verbose_name='ספק')
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = 'supplier categories'

class Unit(models.Model):
    '''
    Common units for products. Examples: 1 kg, 100 grams, bundle, etc.
    '''
    unit = models.CharField(max_length=25)

    def __unicode__(self):
        return self.unit


class ProductManager(models.Manager):
    def get_query_set(self):
        '''
        Get all related objects by default
        '''
        return super(ProductManager, self).get_query_set().select_related('supplier', 'unit').order_by('name')

class Product(models.Model):
    PACKAGED_CHOICES = (
        (0, u'תפזורת'),
        (1, u'ארוז')
    )
    ORGANIC_CHOICES = (
        (0, u'לא אורגני'),
        (1, u'אורגני'),
    )
    
    name = models.CharField(max_length=100, verbose_name='שם המוצר')
    supplier = models.ForeignKey(Supplier, verbose_name='ספק')
    category = models.ForeignKey(SupplierCategory, verbose_name='קטגוריה', null=True, blank=True)
    unit = models.ForeignKey(Unit, verbose_name='יחידה')
    price_before_VAT = decimal_field(null=True, blank=True, verbose_name='מחיר ללא מע"מ')
    price_including_VAT = decimal_field(null=True, blank=True, verbose_name='מחיר כולל מע"מ')
    packaged = models.SmallIntegerField(verbose_name='ארוז/תפזורת', choices=PACKAGED_CHOICES)
    organic  = models.SmallIntegerField(verbose_name='אורגני', choices=ORGANIC_CHOICES)
    package_size = decimal_field(verbose_name='גודל אריזה')                             # all values are integer
    description = models.TextField(blank=True, null=False, verbose_name='תיאור')
    inventory = decimal_field(null=True, blank=True, verbose_name='מלאי')
    deleted = models.BooleanField(default=False)

    objects = ProductManager()
#    all_objects = models.Manager() # TODO: use this in admin page 
    
    class Meta:
        pass
        #unique_together = (('supplier', 'name'),)

    def __unicode__(self):
        return self.name

    def delete(self):
        self.deleted = True # TODO: hook to pre_delete or post_delete signals instead 
        self.save()

    def create_new_sale_product(self, sale):
        return SaleProduct(product=self, sale=sale, price=0, price_before_VAT=self.price_before_VAT,
                           price_including_VAT=self.price_including_VAT, package_size=self.package_size)

# TODO: fix this. currently hack to make things work, using after_VAT price as the single source of truth
@receiver(pre_save, sender=Product)
def calc_product_prices(sender, instance, **kwargs):
#    if instance.price_before_VAT is None and instance.price_including_VAT is not None:
#        instance.price_before_VAT = instance.price_including_VAT / VALUE_ADDED_TAX_RATIO
#    if instance.price_before_VAT is not None: 
#        if instance.price_including_VAT is None:
#            instance.price_including_VAT = instance.price_before_VAT * VALUE_ADDED_TAX_RATIO
#        instance.selling_price = instance.price_including_VAT * SELLING_PRICE_RATIO
    if instance.price_including_VAT is not None:
        instance.price_before_VAT = instance.price_including_VAT / VALUE_ADDED_TAX_RATIO

    elif instance.price_before_VAT is not None:
        instance.price_including_VAT = instance.price_before_VAT * VALUE_ADDED_TAX_RATIO


class SaleProduct(models.Model):
    product = models.ForeignKey(Product)
    sale    = models.ForeignKey(Sale)
    price_before_VAT = decimal_field(null=True, blank=True, verbose_name='מחיר מעודכן ללא מע"מ')
    price_including_VAT = decimal_field(null=True, blank=True, verbose_name='מחיר מעודכן כולל מע"מ')
    package_size = decimal_field()                       # all are integer
    # TODO add optional field estimated_next_purchase (date/in X weeks/months)
    pre_sale_comments = models.TextField(blank=True, null=False, verbose_name='הערות לפני מכירה')
    post_sale_comments = models.TextField(blank=True, null=False, verbose_name='הערות אחרי מכירה')
    amount_ordered = decimal_field(verbose_name='כמות שהוזמנה', null=True, blank=True)
    amount_received = decimal_field(verbose_name='כמות שהגיעה בפועל', null=True, blank=True)
    delivery_amount = decimal_field(verbose_name='כמות לפי תעודת משלוח/חשבונית', null=True, blank=True)
    objects = CustomQuerySetManager()

    def get_amount_ordered(self): # TODO: rename to amount_ordered() once field with same name is removed
        '''Note: using this method without prefetch_related('productfinalorder_set') will trigger extra queries'''
        return sum([order.amount for order in self.productfinalorder_set.all()])
    
    class QuerySet(QuerySet):
        def by_sale(self, sale, *args, **kwargs):
            return self.filter(sale__exact=sale, *args, **kwargs)
        
        def by_supplier(self, supplier, *args, **kwargs):
            return self.filter(product__supplier=supplier, *args, **kwargs)

        def group_by_product_id(self):
            """Return a dictionary, whose keys are Product id and values are lists of SaleProduct's"""
            return dict([(sp.product.id, sp) for sp in self if sp.product is not None])
        
        def get_delivery_sum(self):
            return sum([sale_product.total_delivery_price() for sale_product in self])

    def name(self):
        '''Note: using this method without select_related() will trigger extra queries'''
        return self.product.name
    
    def amount_bought(self):
        '''Note: using this method without prefetch_related('productfinalorder_set') will trigger extra queries'''
        return sum([order.amount_bought for order in self.productfinalorder_set.all()]) 

    def total_delivery_price(self):
        return self.delivery_amount * self.price_including_VAT
    
    def total_received_price(self):
        return self.amount_received * self.price_including_VAT
    
    def __unicode__(self): # TODO: fix to avoid multiple queries
        return self.product.name

# TODO: fix this. currently hack to make things work, using after_VAT price as the single source of truth
@receiver(pre_save, sender=SaleProduct)
def calc_sale_product_prices(sender, instance, **kwargs):
    if instance.price_including_VAT is not None:
        instance.price_before_VAT = instance.price_including_VAT / VALUE_ADDED_TAX_RATIO
        instance.price = instance.price_including_VAT * SELLING_PRICE_RATIO
    elif instance.price_before_VAT is not None:
        instance.price_including_VAT = instance.price_before_VAT * VALUE_ADDED_TAX_RATIO
        instance.price = instance.price_including_VAT * SELLING_PRICE_RATIO

def calc_order_info(products, member_orders):
    totals = {}
    leftovers = {}
    for product in products:
        total = sum([member_order.amount for member_order in member_orders[product.id].values()])
        totals[product.id] = total
        leftover = total % product.package_size
        if leftover == 0:
            leftovers[product.id] = "0"
        else:
            leftovers[product.id] = "+%.1f / -%.1f" %(product.package_size - leftover, leftover)
    return {'totals': totals, 'leftovers': leftovers}

class ProductOrder(models.Model):
    member = models.ForeignKey(User, verbose_name='חבר/ה', editable=False)
    product = models.ForeignKey(SaleProduct, verbose_name='מוצר', editable=False)
    amount = decimal_field(max_digits=12, decimal_places=0, verbose_name='כמות')
    min_amount = decimal_field(max_digits=12, decimal_places=0, verbose_name='כמות מינימלית', default=0)
    max_amount = decimal_field(max_digits=12, decimal_places=0, verbose_name='כמות מקסימלית', default=0)
    comments = models.TextField(blank=True, null=False, verbose_name='הערות')
    
    NON_ZERO_AMOUNT = Q(amount__gt=0)
    objects = CustomQuerySetManager()
    
    class QuerySet(QuerySet):
        def by_sale(self, sale, *args, **kwargs):
            return self.filter(product__sale=sale, *args, **kwargs)
        
        def by_member(self, member, *args, **kwargs):
            return self.filter(member=member, *args, **kwargs)

        def by_supplier(self, supplier, *args, **kwargs):
            if supplier:
                return self.filter(product__product__supplier=supplier, *args, **kwargs)
            else:
                return self

        def by_category(self, category, *args, **kwargs):
            if category:
                return self.filter(product__product__category=category, *args, **kwargs)
            else:
                return self

        def non_zero(self, *args, **kwargs):
            return self.filter(ProductOrder.NON_ZERO_AMOUNT, *args, **kwargs)

        def order_count_per_member(self):
            results = {}
            for member in User.objects.active_members():
                results[member] = 0
            for product_order in self:
                results[product_order.member] += 1
            return results

        def get_member_orders_info(self, sale=None, supplier=None, category=None, add_final_orders=False):
            '''
            Analyze ProductOrder's and retrieve the following information:
            1. which products were ordered
            2. by whom the products were ordered
            '''
            queryset = self
            if sale is not None:
                queryset = queryset.by_sale(sale)
            if supplier is not None:
                queryset = queryset.by_supplier(supplier)
            if category is not None:
                queryset = queryset.by_category(category)
            member_orders = {}
            products = []
            members = set()
            for product_order in queryset:
                product = product_order.product
                if product not in products:
                    products.append(product)
                member = product_order.member
                members.add(member)
                if not member_orders.has_key(product.id):
                    member_orders[product.id] = {}
                member_orders[product.id][member.id] = product_order
            member_dict = dict([(member.id, member) for member in members])
            product_dict = dict([(product.id, product) for product in products])
            result_dict = {'member_orders': member_orders, 'products': products, 'members': members,
                           'member_dict': member_dict, 'product_dict': product_dict,
                           'order_info': calc_order_info(products, member_orders)}
            if add_final_orders:
                final_orders_info_dict = ProductFinalOrder.objects.select_related().get_member_orders_info(sale=sale, supplier=supplier, category=category)
                result_dict.update({'final_orders': final_orders_info_dict['member_orders'],
                                    'draft_info': final_orders_info_dict['order_info']})
            return result_dict
        
    def total_price(self):
        return self.amount * self.product.price

    def member_full_name(self):
        return self.member.get_full_name()
    
    def supplier(self):
        return self.product.product.supplier
    
    def info(self):
        return 'Member: %s Product id: %s amount: %s' %(self.member, self.product.id, self.amount)
    
    def product_name(self):
        return self.product.name()
    
    def __unicode__(self):
        return 'sale %d amount %d' %(self.product.sale_id, self.amount)

class ProductFinalOrder(models.Model):
    product_order = models.ForeignKey(ProductOrder)
    sale_product = models.ForeignKey(SaleProduct, verbose_name='מוצר', editable=False)
    amount = decimal_field(verbose_name='כמות מתוקנת')
    amount_bought = decimal_field(verbose_name='כמות ששולמה בקופה', null=True, blank=True)
    was_bought = models.BooleanField(default=False, verbose_name='המוצר נקנה')
    # TODO: add updated_by, or use django-reversion to keep track of changes in amount
    
    NON_ZERO_AMOUNT_LENIENT = Q(product_order__amount__gt=0) | Q(amount__gt=0)
    NON_ZERO_AMOUNT = Q(amount__gt=0)
    objects = CustomQuerySetManager()

    class QuerySet(QuerySet):
        def by_sale(self, sale, *args, **kwargs):
            return self.filter(product_order__product__sale=sale, *args, **kwargs)
        
        def by_member(self, member, *args, **kwargs):
            return self.filter(product_order__member=member, *args, **kwargs)

        def by_supplier(self, supplier, *args, **kwargs):
            if supplier:
                return self.filter(product_order__product__product__supplier=supplier, *args, **kwargs)
            else:
                return self

        def by_category(self, category, *args, **kwargs):
            if category:
                return self.filter(product_order__product__product__category=category, *args, **kwargs)
            else:
                return self

        def by_sale_product(self, sale_product):
            return self.filter(product_order__product=sale_product)
        
        def non_zero(self, *args, **kwargs):
            return self.filter(ProductFinalOrder.NON_ZERO_AMOUNT, *args, **kwargs)

        def non_zero_lenient(self, *args, **kwargs):
            return self.filter(ProductFinalOrder.NON_ZERO_AMOUNT_LENIENT, *args, **kwargs)

        def amount_differs(self, *args, **kwargs):
            return self.exclude(amount=F('amount_bought'), amount_bought__isnull=False, *args, **kwargs)

        def group_by_member(self):
            queryset = self
            members = []
            member_orders = {}
            for product_final_order in queryset:
                member = product_final_order.product_order.member
                if member not in members:
                    members.append(member)
                    member_orders[member.id] = []
                member_orders[member.id].append(product_final_order)
            return {'members': members, 'orders_by_member': member_orders}

        def group_by_product(self):
            queryset = self
            products = []
            product_orders = {}
            for product_final_order in queryset:
                product = product_final_order.sale_product.product
                if product not in products:
                    products.append(product)
                    product_orders[product.id] = []
                product_orders[product.id].append(product_final_order)
            return {'products': products, 'orders_by_product': product_orders}

        def get_member_orders_info(self, sale=None, supplier=None, category=None): # TODO: consolidate with ProductOrder.get_member_orders_info()
            '''
            Analyze ProductFinalOrder's and retrieve the following information:
            1. which products were ordered
            2. by whom the products were ordered
            '''
            queryset = self
            if sale is not None:
                queryset = queryset.by_sale(sale)
            if supplier is not None:
                queryset = queryset.by_supplier(supplier)
            if category is not None:
                queryset = queryset.by_category(category)
            member_orders = {}
            products = []
            members = []
            for product_final_order in queryset:
                product_order = product_final_order.product_order
                product = product_order.product
                if product not in products:
                    products.append(product)
                member = product_order.member
                if member not in members:
                    members.append(member)
                if not member_orders.has_key(product.id):
                    member_orders[product.id] = {}
                member_orders[product.id][member.id] = product_final_order
            return {'member_orders': member_orders, 'products': products, 'members': members,
                    'order_info': calc_order_info(products, member_orders)}

    def total_price(self):
        return self.amount * self.sale_product.price
    
    def total_price_bought(self):
        return self.amount_bought * self.sale_product.price
    
    def product_name(self):
        return self.product_order.product_name()
    
    def supplier(self):
        return self.product_order.supplier()
    
    def member(self):
        return self.product_order.member

    def member_full_name(self):
        return self.product_order.member_full_name()
    
    def __unicode__(self):
        return 'sale %d amount %d' %(self.sale_product.sale_id, self.amount)

@receiver(pre_save, sender=ProductFinalOrder)
def update_sale_product_field(sender, instance, **kwargs):
    if instance.product_order is not None:
        instance.sale_product = instance.product_order.product

#    def __unicode__(self): # TODO: fix to avoid multiple queries
#        return u'%s [%s kg (was %s kg)]' % (self.product.name, self.amount, self.product_order.amount)

class TransferType(models.Model):
    description = models.CharField(max_length=100, unique=True, verbose_name='תיאור')
    expense = models.BooleanField(default=True, verbose_name='הוצאה')
    supplier = models.ForeignKey(Supplier, blank=True, null=True, verbose_name='ספק')
    user = models.ForeignKey(User, blank=True, null=True, verbose_name='חבר/ה')
    comments = models.TextField(blank=True, null=False, verbose_name='הערות')

    def __unicode__(self):
        return self.description

# TODO: remove
class TransferMethod(models.Model):
    method = models.CharField(max_length=25, unique=True)

# TODO change these constants to something less fragile
PURCHASE_TRANSFER_TYPE = 21
OTHER_INCOME_TYPE = 25
OTHER_EXPENSE_TYPES = (22, 23, 24, 26)

METHOD_CASH = 'cash'
METHOD_CHEQUE = 'cheque'
METHOD_BANK_TRANSFER = 'bank transfer'

class TransferManager(CustomQuerySetManager):
    def get_query_set(self):
        '''
        Get some of the related objects by default
        '''
        return super(TransferManager, self).get_query_set().select_related('type', 'user', 'sale', 'type__supplier')

class Transfer(models.Model):
    METHOD_CHOICES = (
        (METHOD_CASH, 'מזומן'),
        (METHOD_CHEQUE, 'המחאה'),
        (METHOD_BANK_TRANSFER, 'העברה בנקאית'),
    )

    transfer_date = models.DateField(verbose_name='תאריך')
    type = models.ForeignKey(TransferType, verbose_name='סוג')
    method = models.CharField(max_length=15, choices=METHOD_CHOICES, blank=True, null=False, verbose_name='שיטה')
    user = models.ForeignKey(User, blank=True, null=True, verbose_name='חבר/ה')
    sale = models.ForeignKey(Sale, blank=True, null=True, verbose_name='מכירה')
    sum = decimal_field(verbose_name='סכום')
    description = models.CharField(max_length=100, blank=True, null=False, verbose_name='תיאור')
    identifier = models.CharField(max_length=30, blank=True, null=False, verbose_name='מזהה עסקה') # For instance: cheque number, bank transfer number
    made_by = models.ForeignKey(User, blank=True, null=True, editable=False, related_name='made_transfers', verbose_name='עודכן ע"י')

    objects = TransferManager()

    class QuerySet(QuerySet):
        def transfers_to_suppliers(self):
            return self.exclude(type__supplier=None)

        def member_transfers(self):
            return self.filter(type=PURCHASE_TRANSFER_TYPE)

        def other_income(self):
            return self.filter(type=OTHER_INCOME_TYPE)

        def other_expenses(self):
            return self.filter(type__in=OTHER_EXPENSE_TYPES)

        def total(self):
            total = decimal.Decimal(0)
            for transfer in self:
                if transfer.type.expense:
                    total -= transfer.sum
                else:
                    total += transfer.sum
            return total

    def get_member(self):
        if self.user:
            return self.user
        else:
            return self.type.user
    
    def supplier(self):
        return self.type.supplier

class Payment(models.Model):
    '''
    A payment of a member for a specific sale.
    The sum of the payment is calculated once the sale is finalized, and the orders cannot be changed.
    The paid sum is entered at the day of the sale, when the member picks up her order 
    '''
    PAYMENT_METHOD_CHOICES = (
        (METHOD_CASH, 'מזומן'),
        (METHOD_CHEQUE, 'המחאה'),
    )

    sale = models.ForeignKey(Sale, editable=False, verbose_name='מכירה')
    member = models.ForeignKey(User, editable=False, verbose_name='חבר/ה')
    sum = decimal_field(editable=False, verbose_name='סכום לתשלום', default=0) # TODO: remove/update dynamically
    took_order = models.BooleanField(default=False, verbose_name='ההזמנה נלקחה')
    sum_paid = decimal_field(verbose_name='סכום ששולם בפועל', default=0)
    method = models.CharField(max_length=15, choices=PAYMENT_METHOD_CHOICES, verbose_name='שיטת תשלום',
                              default=METHOD_CHEQUE)
    comments = models.TextField(null=False, blank=True, verbose_name='הערות')
    updated_by = models.ForeignKey(User, editable=False, verbose_name='עודכן ע"י', null=True, blank=True, related_name='updated_payments')
    # TODO: add 'verified'/'transfered' field
    # TODO: add 'transfer' foreign key - maybe NULL when not transfered, and previous field is redundant

    def get_sum(self):
        product_final_orders = ProductFinalOrder.objects.by_sale(self.sale).by_member(self.member).select_related()
        return sum([order.total_price_bought() for order in product_final_orders])
        
    objects = CustomQuerySetManager()

    class QuerySet(QuerySet):
        def sum_bought_per_payment(self):
            sale_and_member_to_payment = {}
            for payment in self:
                sale_and_member_to_payment[(payment.sale.id, payment.member.id)] = payment
            product_final_orders = ProductFinalOrder.objects.select_related().filter(Q(amount_bought__gt=0))
            sums = {}
            for product_final_order in product_final_orders:
                try:
                    payment = sale_and_member_to_payment[(product_final_order.sale_product.sale.id, product_final_order.member().id)]
                except:
                    continue
                if not sums.has_key(payment.id):
                    sums[payment.id] = 0
                sums[payment.id] += product_final_order.total_price_bought()
            return sums
    
    def __unicode__(self): # TODO: fix to avoid multiple queries
        return u'Payment<sale: %s user: %s sum paid: %s>' %(self.sale, self.member, self.sum_paid)

class PaymentToSupplier(models.Model):
    '''
    A payment which should be made by the co-op to a certain supplier for a specific sale 
    '''
    sale = models.ForeignKey(Sale, verbose_name='מכירה')
    supplier = models.ForeignKey(Supplier, verbose_name='ספק')
    sum = decimal_field(verbose_name='סכום סופי לתשלום לספק')
    comments = models.TextField(blank=True, null=False, verbose_name='הערות')

    def __repr__(self):
        return '<Sum: %s comments: %s>' %(self.sum, self.comments)

class OtherPayment(models.Model):
    sum = decimal_field(verbose_name='סכום')
    expense = models.BooleanField(default=True, verbose_name='הוצאה')
    date = models.DateField(blank=True, null=True, verbose_name='תאריך')
    description = models.TextField(blank=False, null=False, verbose_name='תיאור')
    comments = models.TextField(blank=True, null=False, verbose_name='הערות')

    class Meta:
        ordering = ('date',)

class ChequeEnvelope(models.Model):
    sale = models.ForeignKey(Sale, related_name='cheque_envelopes')
    internal_id = models.IntegerField(verbose_name='מספר לשימוש פנימי')
    external_id = models.CharField(max_length=20, verbose_name='מזהה חיצוני')

class ChequePayment(models.Model):
    '''
    Details of a payment done in cheque
    '''

    BANK_CHOICES = (
        (12, 'בנק הפועלים'),
        (10, 'בנק לאומי'),
        (11, 'בנק דיסקונט'),
        (20, 'בנק מזרחי טפחות'),
        (31, 'הבנק הבינלאומי הראשון'),
        (17, 'בנק מרכנתיל דיסקונט'),
        (9,  'בנק הדואר'),
        (13, 'בנק איגוד'),
        (4,  'בנק יהב'),
        (14, 'בנק אוצר החייל'),
        (26, 'יובנק'),
        (0, 'אחר'),
    )
    
    payment = models.OneToOneField(Payment, editable=False)
    cheque_number = models.CharField(max_length=20, verbose_name='מספר המחאה')
    account_number = models.CharField(max_length=20, verbose_name='מספר חשבון')
    account_name = models.CharField(max_length=30, verbose_name='בעל/ת חשבון')
#    envelope = models.ForeignKey(ChequeEnvelope, related_name='cheques', verbose_name='מספר מעטפה')
    envelope_internal_id = models.IntegerField(verbose_name='מספר מעטפה')
    bank = models.IntegerField(choices=BANK_CHOICES, verbose_name='בנק', blank=False)

    objects = CustomQuerySetManager()
    
    class QuerySet(QuerySet):
        def by_sale(self, sale, *args, **kwargs):
            return self.filter(payment__sale__exact=sale, *args, **kwargs)

def non_admin():
    return User.objects.exclude(username='admin')

def active_members():
    return User.objects.non_admin().filter(is_active=True)

User.objects.non_admin = non_admin
User.objects.active_members = active_members
