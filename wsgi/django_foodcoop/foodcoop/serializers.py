from rest_framework import serializers

from foodcoop.models import *

__all__ = (
    'OtherPaymentSerializer',
)

class OtherPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = OtherPayment
