from django.conf import settings
from django.contrib.sites.models import Site

from foodcoop.models import Sale, Supplier

def extra(request):
    '''
    Add flags for member role (is_customer, is_treasurer, etc.) and relevant information for these roles.
    For example, a purchasing manager will need to know about her suppliers
    
    A staff member will be granted access to all of the information
    '''
    is_purchase_manager = is_customer = is_treasurer = is_sale_day_manager = False
    managed_suppliers = archived_sales = []
    sale_open_to_buyers = open_sale = sale_draft = ready_sale = None 
    if hasattr(request, 'user') and not request.user.is_anonymous():
        group_names = [group.name for group in request.user.groups.all()]
        is_purchase_manager = ("Purchasing managers" in group_names) or request.user.is_staff
        is_customer = ("Customers" in group_names) or request.user.is_staff
        is_treasurer = ("Treasurers" in group_names) or request.user.is_staff
        is_sale_day_manager = ("Sale day managers" in group_names) or request.user.is_staff
#        if request.user.is_staff:
#            managed_suppliers = Supplier.objects.all()
#        else:
        managed_suppliers = request.user.supplier_set.all()
        sale_draft = Sale.objects.latest_draft()
        ready_sale = Sale.objects.latest_ready()
        archived_sales = Sale.objects.archived()
    if is_purchase_manager:
        sale_open_to_buyers = Sale.objects.latest_open_to_buyers()
    if is_customer:
        open_sale = Sale.objects.latest_open_to_all()
    return {
        'is_purchase_manager': is_purchase_manager,
        'is_customer': is_customer,
        'is_sale_day_manager': is_sale_day_manager,
        'is_treasurer': is_treasurer, 
        'managed_suppliers': managed_suppliers,
        'sale_open_to_buyers': sale_open_to_buyers,
        'open_sale': open_sale,
        'ready_sale': ready_sale,
        'sale_draft': sale_draft,
        'archived_sales': archived_sales,
    }

# Taken from https://github.com/pinax/pinax-utils/blob/master/pinax_utils/context_processors.py
def site_settings(request):
    ctx = {}
    if Site._meta.installed:
        site = Site.objects.get_current()
        ctx.update({
            "SITE_NAME": site.name,
            "SITE_DOMAIN": site.domain,
            "SITE_ENVIRONMENT": settings.ENVIRONMENT,
        })
    return ctx
