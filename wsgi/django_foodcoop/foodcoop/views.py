# -*- coding: utf-8 -*-
import abc
import decimal
import json
import re
from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook

from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import F
from django.views.decorators.cache import never_cache
from django.views.generic.base import View, TemplateView
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView

from extra_views import InlineFormSetView, ModelFormSetView
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView

from foodcoop.models import *
from foodcoop.forms import *
from foodcoop.serializers import *
from foodcoop.utils.decorators import class_view_decorator
from foodcoop.utils.unicodecsv import UnicodeWriter

__all__ = (
    'MembersListView', 'SharesListView', 'TransfersListView', 'MemberPaymentsListView', 'SalePaymentsListView',
    'EditSupplierProductsView', 'SupplierProductListView', 'UpdateSupplierSaleProductsView', 'MemberOrdersView',
    'ManageSaleDayView', 'ManageSaleChequesView', 'CreateTransferView', 'UpdateTransferView', 'DeleteTransferView',
    'MemberOrderFormView', 'PriceListSheetView', 'PaymentsBreakdown', 'MemberOrderListView', 'PurchaseMemberOrdersUpdateView',
    'SupplierSaleOrderSummaryView', 'SupplierSaleOrderSummaryCsvView', 'SupplierSaleProductListFormView', 'SupplierSaleProductListSummaryFormView',
    'MemberFinalOrderListView', 'SaleDayMemberOrdersUpdateView', 'TreasuryUsefulLinks', 'MemberPostOrderListView',
    'AdminArchive', 'PurchaseArchive', 'ImpersonateView', 'NotesPerMemberExcelView', 'NotesPerProductExcelView',  'ProductFinalOrderUpdateView',
    'TreasuryArchive', 'SupplierSaleProductListSummaryView', 'SaleDaySummaryView', 'ProductsInSaleView', 'ProductSaleView', 'MemberArchive',
    'EditSupplierCategoryProductsView', 'TreasurySaleSummary', 'TreasuryMemberFinalOrdersView',
    'TreasuryPostSaleSupplierSummariesView', 'AdminEditSuppliersProducts', 'AdminSaleStatusView',
    'ProductListView', 'OtherPaymentApiListView', 'InventoryListView',
)

class CsvViewMixin(object):
    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def get_header(self):
        pass
    
    @abc.abstractmethod
    def calc_values(self, *args, **kwargs):
        pass
    
    def write_header(self):
        self.writer.writerow(self.get_header())
    
    def write_row_values(self, values):
        self.writer.writerow([unicode(v) for v in values])
    
    def render_to_csv_response(self, filename, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s"' %filename
        self.values = self.calc_values(*args, **kwargs)
        self.writer = UnicodeWriter(response)
        self.write_header()
        [self.write_row_values(line_values) for line_values in self.values]
        return response

class ExcelViewMixin(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def calc_excel_values(self, *args, **kwargs):
        return []
    
    def render_to_excel_response(self, filename, *args, **kwargs):
        workbook = Workbook(encoding='utf-8')
        worksheet = workbook.get_active_sheet()
        values = self.calc_excel_values(self, *args, **kwargs)
        for row in range(len(values)):
            for col in range(len(values[row])):
                worksheet.cell(row=row, column=col).value = values[row][col] 
        response = HttpResponse(save_virtual_workbook(workbook), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') #content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="%s"' %filename
        return response

class SupplierSaleOrderSummaryViewMixin(object):
    def init_args(self, sale_id, supplier_id):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        self.supplier = get_object_or_404(Supplier, pk=supplier_id)

    def get_order_summary(self):
        product_final_orders = ProductFinalOrder.objects.by_sale(self.sale).by_supplier(self.supplier).non_zero() \
                                .select_related().order_by('product_order__product__product__id')
        if product_final_orders.count() == 0:
            return []
        orders = []
        order = None
        latest_product_id = None
        for product_final_order in product_final_orders:
            sale_product = product_final_order.product_order.product
            product = sale_product.product

            if sale_product.id != latest_product_id:
                # add old order, start new order
                if order is not None:
                    orders.append(order)
                order = {'name': product.name, 'unit': product.unit,
                         'price_before_VAT': sale_product.price_before_VAT,
                         'purchase_price': sale_product.price_including_VAT,
                         'amount': product_final_order.amount}
                latest_product_id = sale_product.id
            else: # aggregate to existing order
                amount = product_final_order.amount - product.inventory
                order['amount'] += amount if amount > 0 else 0
        if order['amount'] > 0:
            orders.append(order)
        # calculate totals
        for order in orders:
            if order['amount'] == int(order['amount']):
                order['amount'] = int(order['amount'])
            order['total_price_before_VAT'] = order['amount'] * order['price_before_VAT']
            order['total_price'] = order['amount'] * order['purchase_price']
        return orders

    def calc_total(self, orders):
        total = {'before_VAT': decimal.Decimal(0), 'including_VAT': decimal.Decimal(0)}
        for order in orders:
            total['including_VAT'] += order['total_price']
            total['before_VAT'] += order['total_price_before_VAT']
        total['doublecheck'] = total['before_VAT'] * VALUE_ADDED_TAX_RATIO
        return total

@class_view_decorator(never_cache)
class NotesPerMemberExcelView(ExcelViewMixin, TemplateView):
    template_name = 'sale_day/notes_per_member.html'
    
    def dispatch(self, request, sale_id, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        self.member_orders_info = ProductFinalOrder.objects.select_related().by_sale(self.sale).non_zero().group_by_member()
        return super(NotesPerMemberExcelView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(NotesPerMemberExcelView, self).get_context_data(**kwargs)
        context.update({
            'sale': self.sale,
        })
        context.update(self.member_orders_info)
        return context

    def post(self, request, *args, **kwargs):
        return self.render_to_excel_response(filename='member-notes.xlsx')
    
    def calc_excel_values(self, *args, **kwargs):
        values = []
        members = self.member_orders_info['members']
        orders_by_member = self.member_orders_info['orders_by_member']
        for member in members:
            values.append([u'שם חבר/ה', member.get_full_name()])
            values.append([u'מוצר',
                           u'ספק',
                           u'יחידה',
                           u'מחיר ליחידה',
                           u'כמות מתוקנת',
                           ])
            for product_final_order in orders_by_member[member.id]:
                product_order = product_final_order.product_order
                sale_product = product_order.product
                product = sale_product.product
                values.append([product.name, product.supplier.name, product.unit.unit,
                               sale_product.price,
                               product_final_order.amount])
            values.append([])
        return values

@class_view_decorator(never_cache)
class NotesPerProductExcelView(ExcelViewMixin, TemplateView):
    template_name = 'sale_day/notes_per_product.html'
    
    def dispatch(self, request, sale_id, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        self.products_orders_info = ProductFinalOrder.objects.select_related().by_sale(self.sale).non_zero().group_by_product()
        return super(NotesPerProductExcelView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(NotesPerProductExcelView, self).get_context_data(**kwargs)
        context.update({
            'sale': self.sale,
        })
        context.update(self.products_orders_info)
        return context

    def post(self, request, *args, **kwargs):
        return self.render_to_excel_response(filename='product-notes.xlsx')
    
    def calc_excel_values(self, *args, **kwargs):
        values = []
        products = self.products_orders_info['products']
        orders_by_product = self.products_orders_info['orders_by_product']
        for product in products:
            values.append([product.name,product.supplier.name,product.unit.unit,u'ארוז' if product.packaged else u'לא ארוז',
                           u'אורגני' if product.organic else u'לא אורגני', u'מחיר:'+str(product.selling_price)])
            values.append([u'שם חבר/ה',
                           u'כמות שהוזמנה',
                           u'כמות מתוקנת',
                           ])
            for product_final_order in orders_by_product[product.id]:
                product_order = product_final_order.product_order
                member = product_final_order.product_order.member
                values.append([member.get_full_name(),
                               product_order.amount,
                               product_final_order.amount])
            values.append([])
        return values
    
@class_view_decorator(never_cache)
class SupplierSaleOrderSummaryCsvView(SupplierSaleOrderSummaryViewMixin, CsvViewMixin, View):
    def get(self, request, sale_id, supplier_id):
        self.init_args(sale_id, supplier_id)
        self.orders = self.get_order_summary()
        return self.render_to_csv_response(filename='order-summary.csv')
    
    def get_header(self):
        return [u'מוצר',
                u'יחידה',
                u'מחיר ליחידה לפני מע"מ',
                u'מחיר ליחידה כולל מע"מ',
                u'מספר יחידות',
                u'סה"כ']
    
    def calc_values(self, *args, **kwargs):
        keys = ['name', 'unit', 'price_before_VAT', 'purchase_price', 'amount', 'total_price']
        values = [[order[key] for key in keys] for order in self.orders]
        return values

@class_view_decorator(never_cache)
class SupplierSaleProductListSummaryView(ListView):
    '''
    Very similar to SupplierSaleProductListSummaryFormView, but in read-only mode.
    
    Allows editing of the correct final total which should be paid to the supplier, in case there 
    are minor inconsistencies with the automatic calculation of the final payment  
    '''
    template_name = 'purchasing_management/sale_product_list_summary_form.html'

    def dispatch(self, request, sale_id, supplier_id):
        self.sale = get_object_or_404(Sale, pk=self.kwargs['sale_id'])
        self.supplier = get_object_or_404(Supplier, pk=self.kwargs['supplier_id'])
        return super(SupplierSaleProductListSummaryView, self).dispatch(request)

    def post(self, request, *args, **kwargs):
        self.form = PaymentToSupplierForm(request.POST)
        if self.form.is_valid():
            instance = PaymentToSupplier.objects.get(sale=self.sale, supplier=self.supplier) # should already exist
            instance.sum = self.form.instance.sum
            instance.comments = self.form.instance.comments
            instance.save()
        return super(SupplierSaleProductListSummaryView, self).get(request, *args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        instance, created = PaymentToSupplier.objects.get_or_create(sale=self.sale, supplier=self.supplier,
                                                                    defaults={'sum': 0})
        if created:
            instance.sum = self.get_queryset().get_delivery_sum()
            instance.save()
        self.form = PaymentToSupplierForm(instance=instance)
        return super(SupplierSaleProductListSummaryView, self).get(request, *args, **kwargs)

    def get_queryset(self):
        return SaleProduct.objects.by_sale(self.sale).by_supplier(self.supplier) \
                .select_related().prefetch_related('productfinalorder_set').order_by('product__id')

    def get_context_data(self, **kwargs):
        context = super(SupplierSaleProductListSummaryView, self).get_context_data(**kwargs)
        context.update({
            'sale': self.sale, 'supplier': self.supplier, 'form': self.form,
            'amount_differs_orders': ProductFinalOrder.objects.by_sale(self.sale).by_supplier(self.supplier) \
                .select_related().amount_differs(), # TODO: this triggers a lot of queries for some reason
            'readonly': True,
        })
        return context

@class_view_decorator(never_cache)
class SupplierSaleProductListSummaryFormView(ModelFormSetView):
    model = SaleProduct
    template_name = 'purchasing_management/sale_product_list_summary_form.html'
    form_class = PostOrderSaleProductForm
    extra = 0
    
    def dispatch(self, request, sale_id, supplier_id):
        self.sale = get_object_or_404(Sale, pk=self.kwargs['sale_id'])
        self.supplier = get_object_or_404(Supplier, pk=self.kwargs['supplier_id'])
        return super(SupplierSaleProductListSummaryFormView, self).dispatch(request)

    def get_queryset(self):
        return SaleProduct.objects.by_sale(self.sale).by_supplier(self.supplier) \
                .select_related().order_by('product__id')

    def get_context_data(self, **kwargs):
        context = super(SupplierSaleProductListSummaryFormView, self).get_context_data(**kwargs)
        context.update({
            'sale': self.sale, 'supplier': self.supplier,
            'amount_differs_orders': ProductFinalOrder.objects.by_sale(self.sale).by_supplier(self.supplier) \
                .select_related().amount_differs(), # TODO: this triggers a lot of queries for some reason
        })
        return context

@class_view_decorator(never_cache)
class SupplierSaleProductListFormView(ModelFormSetView):
    model = SaleProduct
    template_name = 'purchasing_management/sale_product_list_update_form.html'
    form_class = SaleProductForm
    extra = 0
    #exclude = ('selling_price', 'deleted')
    
    def dispatch(self, request, sale_id, supplier_id):
        self.sale = get_object_or_404(Sale, pk=self.kwargs['sale_id'])
        self.supplier = get_object_or_404(Supplier, pk=self.kwargs['supplier_id'])
        return super(SupplierSaleProductListFormView, self).dispatch(request)
        
    def get_queryset(self):
        return SaleProduct.objects.by_sale(self.sale).by_supplier(self.supplier) \
                .select_related().order_by('product__id')

    def get_success_url(self):
        if '_redirect_to_summary' not in self.request.POST:
            return super(SupplierSaleProductListFormView, self).get_success_url()
        else:
            return reverse('supplier-sale-order-summary', args=(self.sale.id, self.supplier.id))
        
    def get_context_data(self, **kwargs):
        context = super(SupplierSaleProductListFormView, self).get_context_data(**kwargs)
        context.update({'sale': self.sale, 'supplier': self.supplier})
        return context
    
@class_view_decorator(never_cache)
class SupplierSaleOrderSummaryView(SupplierSaleOrderSummaryViewMixin, TemplateView):
    template_name = 'purchasing_management/sale_order_from_supplier_summary.html'

    def get(self, request, sale_id, supplier_id):
        self.init_args(sale_id, supplier_id)
        return super(SupplierSaleOrderSummaryView, self).get(request)

    def get_context_data(self, **kwargs):
        context = super(SupplierSaleOrderSummaryView, self).get_context_data(**kwargs)
        orders = self.get_order_summary()
        total = self.calc_total(orders)
        context.update({'sale': self.sale, 'supplier': self.supplier,
                        'orders': orders, 'total': total})
        return context

# TODO: refactor (move to models.py and change completely - see group_by_product_id() in models.py)
def _sale_product_orders(sale, final=False):
    sale_products = sale.saleproduct_set.select_related().all()
    members = set()
    product_order_dict = {}
    for sale_product in sale_products:
        if final:
            product_orders = ProductFinalOrder.objects.filter(product_order__product=sale_product).select_related().all()
        else:
            product_orders = sale_product.productorder_set.select_related().all()
        for product_order in product_orders:
            member = product_order.member # this doesn't work for ProductFinalOrder
            members.add(member)
            if not product_order_dict.has_key(member.id):
                product_order_dict[member.id] = {}
            product_order_dict[member.id][sale_product.id] = product_order
    return sale_products, members, product_order_dict
    
class SaleInfo(object):
    def __init__(self, products, members, member_orders, member_order_objects, totals, amounts, leftovers, min_amounts, max_amounts):
        self.products = products
        self.members = members
        self.member_orders = member_orders
        self.member_order_objects = member_order_objects
        self.totals = totals
        self.amounts = amounts
        self.leftovers = leftovers
        self.min_amounts = min_amounts
        self.max_amounts = max_amounts
    
    def __repr__(self):
        return repr(self.member_order_objects)

    def __unicode__(self):
        return repr(self)
    
# TODO: refactor. this is too complicated
def sale_info(sale, supplier=None, final=False):
    products = sale.saleproduct_set.select_related()
    if supplier is not None:
        products = products.filter(product__supplier=supplier).order_by('product__id')
    products = products.all()
    members = set()
    member_orders = {}
    member_order_objects = {}
    totals = {}
    amounts = {}
    min_amounts = {}
    max_amounts = {}
    leftovers = {}
    for product in products:
        if final:
            product_orders = ProductFinalOrder.objects \
                                .filter(product_order__product=product) \
                                .select_related('product_order', 'product_order__member').all()
        else:
            product_orders = product.productorder_set.select_related('member').all()
        total = 0
        for product_order in product_orders:
            member = product_order.product_order.member if final else product_order.member
            if member.username == 'admin': # another patch to the collection...
                continue
            members.add(member)
            if not member_orders.has_key(member.id):
                member_orders[member.id] = {}
                member_order_objects[member.id] = {}
            member_orders[member.id][product.id] = product_order.amount
            member_order_objects[member.id][product.id] = product_order
            total += product_order.amount
            if not final:
                if not min_amounts.has_key(member.id):
                    min_amounts[member.id] = {}
                    max_amounts[member.id] = {}
                min_amounts[member.id][product.id] = product_order.min_amount
                max_amounts[member.id][product.id] = product_order.max_amount
        amounts[product.id] = product.package_size
        totals[product.id] = total
        leftover = total % product.package_size
        if leftover == 0:
            leftovers[product.id] = "0"
        else:
            leftovers[product.id] = "+%.1f / -%.1f" %(product.package_size - leftover, leftover)
    return SaleInfo(products, members, member_orders, member_order_objects, totals, amounts, leftovers, min_amounts, max_amounts)

def sale_final_info(sale, supplier=None):
    return sale_info(sale, supplier=supplier, final=True)
  
def update_product_final_order(order_info, draft_info, product_id, member_id, amount):
    #product_final_order = ProductFinalOrder.objects.filter(product_order__product=product).filter(product_order__member=member)
    product_order = order_info.member_order_objects.get(member_id, {}).get(product_id, None)
    product_final_order = draft_info.member_order_objects.get(member_id, {}).get(product_id, None)
    if product_final_order: # update amount of existing order
        if product_final_order.amount != amount:
            product_final_order.amount = amount
            product_final_order.save()
    elif amount > 0:
        assert product_order is not None, 'Problem with product order for product id %s and member id %s amount %s' %(product_id, member_id, amount)
        #raise Http404("This shouldn't happen: product id %s member id %s amount %s" %(product.id, member.id, amount))
        product_final_order = ProductFinalOrder.objects.create(amount=amount, product_order=product_order)
    #product_final_order.save()

@class_view_decorator(never_cache)
class MemberOrderFormView(ModelFormSetView):
    model = ProductOrder
    #template_name = 'members/test.html'
    template_name = 'members/member_order_form.html'
    form_class = ProductOrderForm
    extra = 0
    #exclude = ('selling_price', 'deleted')

    def dispatch(self, request, sale_id, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        if not self.sale.is_open():
            raise Http404(u"Sale %s is not open for member orders" %(self.sale))
        self.member = request.user
        return super(MemberOrderFormView, self).dispatch(request, *args, **kwargs)
            
    def get_queryset(self):
        return ProductOrder.objects.select_related().by_sale(self.sale).by_member(member=self.member) \
            .order_by('product__product__supplier', 'product__product__name')

    def get_context_data(self, **kwargs):
        context = super(MemberOrderFormView, self).get_context_data(**kwargs)
        context.update({'sale': self.sale, 'member': self.member})
        return context
    
    def get_product_orders_by_product(self):
        product_orders = self.get_queryset()
        product_orders_by_product = {}
        for product_order in product_orders:
            product_orders_by_product[product_order.product.id] = product_order
        return product_orders_by_product
    
    def get_initial(self):
        '''
        Create a ProductOrder for each SaleProduct which is included in this Sale, if does not exist.
        The default amount will be zero.
        '''
        sale_products = self.sale.saleproduct_set.all()
        product_orders = self.get_product_orders_by_product()
        initial = []
        for sale_product in sale_products:
            if product_orders.has_key(sale_product.id):
                product_order = product_orders.get(sale_product.id)
            else: # create an initial order to this product, with amount zero
                product_order = ProductOrder(member=self.member, product=sale_product, amount=0)
                product_order.save() # TODO: use bulk_create instead
            form = ProductOrderForm(instance=product_order)
            initial.append(form.initial)
        return initial

# TODO: refactor - mainly post()
class BasePurchaseMemberOrdersUpdateView(TemplateView):
    pass

@class_view_decorator(never_cache)
class PurchaseMemberOrdersUpdateView(BasePurchaseMemberOrdersUpdateView):
    #model = ProductFinalOrder
    template_name = 'purchasing_management/member_orders_from_supplier.html'
    #form_class = ProductFinalOrderForm
    extra = 0

    def dispatch(self, request, sale_id, supplier_id=None, category_id=None, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        if supplier_id is None and category_id is None:
            raise Http404('Either supplier ID or category ID should be provided')
        self.supplier = get_object_or_404(Supplier, pk=supplier_id) if supplier_id else None
        self.category = get_object_or_404(SupplierCategory, pk=category_id) if category_id else None
        self.product_orders = ProductOrder.objects.by_sale(self.sale) \
            .by_supplier(self.supplier).by_category(self.category).select_related().all()
        self.order_info = self.product_orders.get_member_orders_info(sale=self.sale, supplier=self.supplier,
                                                                     category=self.category,
                                                                     add_final_orders=self.sale.is_draft())
        self.form_errors = []
        return super(PurchaseMemberOrdersUpdateView, self).dispatch(request)

    def get_context_data(self, **kwargs):
        context = super(PurchaseMemberOrdersUpdateView, self).get_context_data(**kwargs)
        context.update({'sale': self.sale, 'supplier': self.supplier,
                        'category': self.category, 'form_errors': self.form_errors})
        context.update(self.order_info)
        return context
    
    def update_product_final_order(self, product_id, member_id, amount):
        #product_final_order = ProductFinalOrder.objects.filter(product_order__product=product).filter(product_order__member=member)
        product_order = self.order_info['member_orders'].get(product_id, {}).get(member_id, None)
        product_final_order = self.order_info['final_orders'].get(product_id, {}).get(member_id, None)
        if product_final_order: # update amount of existing order
            if product_final_order.amount != amount:
                product_final_order.amount = amount
                product_final_order.save()
        elif amount > 0:
            assert product_order is not None, 'Problem with product order for product id %s and member id %s amount %s' %(product_id, member_id, amount)
            product_final_order = ProductFinalOrder.objects.create(amount=amount, product_order=product_order)

    def post(self, request, *args, **kwargs):
        assert not self.sale.is_open()
        for k,v in request.POST.items():
            m = re.match("product_(\\d+)_member_(\\d+)", k)
            if m:
                product_id = int(m.group(1))
                member_id = int(m.group(2))
                try:
                    amount = decimal.Decimal(v)
                except decimal.InvalidOperation:
                    product = self.order_info['product_dict'].get(product_id)
                    member = self.order_info['member_dict'].get(member_id)
                    error_message = u'כמות לא חוקית עבור מוצר "%s" וחבר/ה %s: "%s"' %(product.product.name, member.get_full_name(), v)
                    self.form_errors.append(error_message)
                    continue
                self.update_product_final_order(product_id, member_id, amount)
        if self.form_errors:
            return self.render_to_response(self.get_context_data())
#        response_data = {'status': 'success'}
#        return HttpResponse(json.dumps(response_data), mimetype="application/json")
        if self.category:
            return HttpResponseRedirect(reverse('purchase-member-orders-form-by-category', args=(self.sale.id, self.category.id)))
        else:
            return HttpResponseRedirect(reverse('purchase-member-orders-form-by-supplier', args=(self.sale.id, self.supplier.id)))

@login_required
def sale_draft(request, sale_id):
    sale = get_object_or_404(Sale, pk=sale_id)
    if sale.is_open():
        sale.status = Sale.DRAFT
        products, members, product_orders = _sale_product_orders(sale, final=False)
        dummy_products, dummy_members, product_final_orders = _sale_product_orders(sale, final=True)
#        updated_orders = []
        for member in members:
            for product in products:
                product_order = product_orders.get(member.id, {}).get(product.id)
                product_final_order = product_final_orders.get(member.id, {}).get(product.id)
                if product_final_order is None and product_order is not None:
                    ProductFinalOrder.objects.create(amount=product_order.amount, product_order=product_order)
        sale.save()
    return HttpResponse("Sale %d is now in draft status" %sale.id, mimetype="text/plain")

# TODO make available to staff only using @user_passes_test(staff_check) or some other way
@login_required
def sale_finalize(request, sale_id):
    sale = get_object_or_404(Sale, pk=sale_id)
    # TODO: move code to models (Sale/Payment)
    payments = []
    if sale.is_draft():
        sale_info = sale_final_info(sale) # TODO: get rid of this
        products, members, dummy, totals = sale_info.products, sale_info.members, sale_info.member_orders, sale_info.totals
        sale.status = Sale.FINAL
        for member in members:
            payment,created = Payment.objects.get_or_create(sale=sale, member=member)
#            #payment.sum = payment_sums[member.id]
            payments.append(payment)
        for product in products:
            product.amount_ordered = product.delivery_amount = product.amount_received = totals[product.id]
        product_final_orders = ProductFinalOrder.objects.by_sale(sale)
        [payment.save() for payment in payments]
        [product.save() for product in products]
        product_final_orders.update(amount_bought=F('amount'))
        sale.save()
        return HttpResponse("Finalized sale %d" %sale.id, mimetype="text/plain")
    else:
        raise Http404(u"Sale %s cannot be finalized. Status is %s" %(sale, sale.status))

@login_required
def sale_close(request, sale_id):
    sale = get_object_or_404(Sale, pk=sale_id)
    if sale.is_finalized():
        sale.status = Sale.CLOSED
        sale.save()
        return HttpResponse("Closed sale %d" %sale.id, mimetype="text/plain")
    else:
        raise Http404(u"Sale %s cannot be closed. Status is %s" %(sale, sale.status))

@login_required
def sale_open(request, sale_id):
    sale = get_object_or_404(Sale, pk=sale_id)
    if sale.status == Sale.OPEN_TO_BUYERS:
        sale.status = Sale.OPEN_TO_ALL
        sale.save()
        return HttpResponse("Opened sale %d" %sale.id, mimetype="text/plain")
    else:
        raise Http404(u"Sale %s cannot be opened. Status is %s" %(sale, sale.status))

@class_view_decorator(never_cache)
class ManageSaleDayView(TemplateView):
    '''
    List payments due by all members for a specific sale
    ''' 
    template_name = 'sale_day/manage_sale.html'

    def get(self, request, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=self.kwargs['sale_id'])
        if not self.sale.is_finalized():
            raise Http404(u"Sale %s is not finalized" %(self.sale))
        return super(ManageSaleDayView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ManageSaleDayView, self).get_context_data(**kwargs)
        context['sale']   = self.sale
        context['payments'] = Payment.objects.select_related().filter(sale=self.sale)
        return context

class ProductsInSaleView(TemplateView):
    '''
    List products for a specific sale
    ''' 
    template_name = 'sale_day/products_in_sale.html'

    def get(self, request, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=self.kwargs['sale_id'])
        return super(ProductsInSaleView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ProductsInSaleView, self).get_context_data(**kwargs)
        context['sale']   = self.sale
        context['sale_products'] = self.sale.saleproduct_set.select_related().all()
        return context

class ProductSaleView(TemplateView):
    '''
    List orders by members for a specific product in a specific sale
    ''' 
    template_name = 'sale_day/product_sale.html'

    def get(self, request, sale_id, saleproduct_id, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        self.saleproduct = get_object_or_404(SaleProduct, pk=saleproduct_id)
        return super(ProductSaleView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ProductSaleView, self).get_context_data(**kwargs)
        context['sale']   = self.sale
        context['saleproduct'] = self.saleproduct
        context['finalorders'] = self.saleproduct.productfinalorder_set.all()
        return context

class SaleDaySummaryView(ManageSaleDayView):
    def get_context_data(self, **kwargs):
        context = super(SaleDaySummaryView, self).get_context_data(**kwargs)
        context['readonly'] = True
        return context

@class_view_decorator(never_cache)
class MembersListView(ListView):
    model = UserProfile
    queryset = UserProfile.objects.prefetch_related('user', 'user__persons') \
                .filter(user__is_active=True).exclude(user__username='admin').order_by('user_number')
    template_name = 'foodcoop/member_list.html'
    
@class_view_decorator(never_cache)
class SharesListView(ListView):
    model = User
    template_name = 'foodcoop/share_list.html'

@class_view_decorator(never_cache)
class CreateTransferView(CreateView):
    model = Transfer
    template_name = 'treasury/transfer_create_form.html'
    success_url = reverse_lazy('transfer-list')

@class_view_decorator(never_cache)
class UpdateTransferView(UpdateView):
    model = Transfer
    template_name = 'treasury/transfer_update_form.html'
    success_url = reverse_lazy('transfer-list')
    
#    return reverse('transfer-list')
#        def get_success_url(self):
    
@class_view_decorator(never_cache)
class DeleteTransferView(DeleteView):
    model = Transfer
    template_name = 'treasury/transfer_confirm_delete.html'
    success_url = reverse_lazy('transfer-list')

@class_view_decorator(never_cache)
class TransfersListView(TemplateView):
    template_name = 'treasury/transfer_list.html'

    def calc_total(self, transfers):
        total = 0
        for transfer in transfers:
            if transfer.type.expense:
                total -= transfer.sum
            else:
                total += transfer.sum
        return total

    def get_context_data(self, **kwargs):
        context = super(TransfersListView, self).get_context_data(**kwargs)
        transfers = Transfer.objects.order_by('transfer_date')
        context['object_list'] = transfers.all()
        context['total'] = transfers.total()
        context['form'] = self.form
        return context

    def post(self, request, *args, **kwargs):
        self.form = TransferForm(request.POST)
        if self.form.is_valid():
            transfer = self.form.save()
            return HttpResponseRedirect(reverse('transfer-list'))
        else:
            return super(TransfersListView, self).get(request, *args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        self.form = TransferForm()
        return super(TransfersListView, self).get(request, *args, **kwargs)
    
@class_view_decorator(never_cache)
class PaymentsBreakdown(TemplateView):
    template_name = "treasury/payments_breakdown.html"
    
    def query_payments(self):
        return Payment.objects.select_related().prefetch_related('chequepayment').all()
    
    def get_context_data(self, **kwargs):
        context = super(PaymentsBreakdown, self).get_context_data(**kwargs)
        payments = self.query_payments()
        context.update({'payment_list': payments,
                        'sum_bought_per_payment': payments.sum_bought_per_payment(),
                        'sale_list': Sale.objects.all(), 'member_list':User.objects.non_admin()})
        return context

@class_view_decorator(never_cache)
class MemberPaymentsListView(PaymentsBreakdown):
    def query_payments(self):
        return super(MemberPaymentsListView, self).query_payments().filter(member=self.member)

    def dispatch(self, request, member_id, *args, **kwargs):
        self.member = get_object_or_404(User, pk=member_id)
        return super(MemberPaymentsListView, self).dispatch(request, *args, **kwargs)

@class_view_decorator(never_cache)
class SalePaymentsListView(PaymentsBreakdown):
    def query_payments(self):
        return super(SalePaymentsListView, self).query_payments().filter(sale=self.sale)

    def dispatch(self, request, sale_id, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        return super(SalePaymentsListView, self).dispatch(request, *args, **kwargs)

class BaseEditSupplierProductsView(InlineFormSetView):
    # TODO: rewrite using http://stackoverflow.com/questions/15203207/prevent-django-from-querying-for-foreignkey-options-for-every-form-in-modelforms
    inline_model = Product
    template_name = 'purchasing_management/edit_products.html'
    exclude = ('selling_price', 'price_before_VAT', 'deleted', 'category', 'supplier')
    
#    formset_class = ProductFormSet
#    form_class = ProductForm
    
#    def get_formset_class(self):
#        return 

#    def get_formset(self):
##        return modelformset_factory(self.model, **self.get_factory_kwargs())
##        raise Exception('in get_formset')
#        return ProductFormFactory(self.model, **self.get_factory_kwargs())
    
#    def get_factory_kwargs(self):
#        raise Exception('in get_factory_kwargs')

@class_view_decorator(never_cache)
class EditSupplierProductsView(BaseEditSupplierProductsView):
    model = Supplier

@class_view_decorator(never_cache)
class EditSupplierCategoryProductsView(BaseEditSupplierProductsView):
    model = SupplierCategory

#    def dispatch(self, request, *args, **kwargs):
#        raise Exception('in dispatch()')

#    def save_new(self, form, commit=True):
#        adsfjkl
#        new_instance = form.save(commit=False)
#        new_instance.supplier = self.object.supplier
#        if commit:
#            new_instance.save()
#        return new_instance

#    def save_new_objects(self, commit=True):
#        raise Exception('in save_new_objects()')
#
#    def save(self, commit=True):
#        raise Exception('in save()')

@class_view_decorator(never_cache)
class SupplierProductListView(ListView):
    model = Product
    template_name = 'purchasing_management/product_list.html'

    def get_context_data(self, **kwargs): # TODO: find out how admin page find field names
        context = super(SupplierProductListView, self).get_context_data(**kwargs)
#        context['fields'] = ('name', 'unit', 'price before VAT', 'price including VAT', 'selling price',
#                             'packaged', 'organic', 'package size', 'description')
        context['fields'] = ('שם המוצר', 'יחידה', 'מחיר ללא מע"מ', 'מחיר כולל מע"מ', 'מחיר מכירה',
                             'ארוז/תפזורת', 'אורגני', 'גודל אריזה', 'תיאור והערות')
        context.update({'supplier': self.supplier, 'sale': self.sale, 'category': self.category})
        if self.sale:
            context['existing_products'] = self.sale_products_by_product_id
        return context
    
    def get(self, request, sale_id=None, pk=None, category_id=None, *args, **kwargs):
        self.supplier = get_object_or_404(Supplier, pk=pk) if pk is not None else None
        self.category = get_object_or_404(SupplierCategory, pk=category_id) if category_id is not None else None
        if self.supplier is None and self.category is None:
            raise Http404("Supplier or supplier category should be specified")
        self.sale = self.get_sale(sale_id)
        return super(SupplierProductListView, self).get(request, *args, **kwargs)
    
    def get_sale(self, sale_id):
        if sale_id is not None:
            return get_object_or_404(Sale, pk=sale_id)
        elif self.request.REQUEST.has_key('sale_id'):
            return get_object_or_404(Sale, pk=self.request.REQUEST['sale_id'])
        else:
            return Sale.objects.latest_open_to_buyers()

    def get_queryset(self):
        if self.sale:
            self.sale_products_by_product_id = SaleProduct.objects.select_related().by_sale(self.sale).group_by_product_id()
        if self.category:
            return Product.objects.filter(category=self.category, deleted=False)
        else:
            return Product.objects.filter(supplier=self.supplier, deleted=False)

@class_view_decorator(never_cache)
class UpdateSupplierSaleProductsView(View):
    def update_selection(self):
        self.new_products = []
        self.deleted_products = []
        self.products = Product.objects.filter(supplier__exact=self.supplier).in_bulk(self.selection_dict.keys())
        self.sale_products = SaleProduct.objects.by_sale(self.sale)
        for (product_id, is_selected) in self.selection_dict.items():
            product = self.products[int(product_id)]
            try:
                matching_sale_product = self.sale_products.get(product__exact=product)
            except ObjectDoesNotExist:
                matching_sale_product = None
            if is_selected and matching_sale_product is None:
                self.new_products.append(product.create_new_sale_product(sale=self.sale))
            elif (not is_selected) and matching_sale_product:
                self.deleted_products.append(matching_sale_product)
        [sale_product.delete() for sale_product in self.deleted_products]   # delete one by one
        SaleProduct.objects.bulk_create(self.new_products)                  # save in bulk

    def get(self, request, *args, **kwargs):
        self.supplier = get_object_or_404(Supplier, pk=self.kwargs['pk'])
        self.sale = get_object_or_404(Sale, pk=request.REQUEST['sale_id'])
        self.selection_dict = json.loads(request.REQUEST['selection']) 
        self.update_selection()
        response_data = {'status': 'success', 'message': '%d מוצרים נוספו, %d מוצרים הוסרו' %(len(self.new_products), len(self.deleted_products))}
        return HttpResponse(json.dumps(response_data), mimetype="application/json")
    
    def post(self, request, *args, **kwargs):
        return self.get(request, *args, **kwargs)

@class_view_decorator(never_cache)
class MemberOrderListView(ListView):
    '''
    Display a list of a member's orders for a specific sale
    '''
    template_name = 'members/order_list.html'
    
    def get(self, request, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=self.kwargs['pk'])
        if self.request.REQUEST.has_key('member_id'):
            self.member = get_object_or_404(User, pk=self.request.REQUEST['member_id'])
        else:
            self.member = request.user
        return super(MemberOrderListView, self).get(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(MemberOrderListView, self).get_context_data(**kwargs)
        context['sale']   = self.sale
        context['member'] = self.member
        return context
    
    def get_queryset(self):
        return ProductOrder.objects.select_related().by_sale(self.sale).by_member(self.member).non_zero() \
            .order_by('product__product__supplier', 'product__product__name')

@class_view_decorator(never_cache)
class MemberFinalOrderListView(ListView):
    '''
    Display a list of a member's orders for a specific sale
    '''
    template_name = 'members/final_order_list.html'
    
    def get(self, request, sale_id, member_id, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        self.member = get_object_or_404(User, pk=member_id)
        return super(MemberFinalOrderListView, self).get(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(MemberFinalOrderListView, self).get_context_data(**kwargs)
        context['sale']   = self.sale
        context['member'] = self.member
        return context
    
    def get_queryset(self):
        return ProductFinalOrder.objects.select_related().by_sale(self.sale).by_member(self.member).non_zero_lenient() \
            .order_by('product_order__product__product__supplier', 'product_order__product__product__name')

@class_view_decorator(never_cache)
class MemberPostOrderListView(MemberFinalOrderListView):
    def get_context_data(self, **kwargs):
        context = super(MemberPostOrderListView, self).get_context_data(**kwargs)
        context['epilogue'] = True
        return context

@class_view_decorator(never_cache)
class SaleDayMemberOrdersUpdateView(ModelFormSetView):
    model = ProductFinalOrder
    template_name = 'sale_day/member_orders_update_form.html'
    form_class = SaleDayProductFinalOrderForm
    extra = 0

    def get_queryset(self):
        return ProductFinalOrder.objects.select_related().by_sale(self.sale).by_member(self.member).non_zero_lenient() \
            .order_by('product_order__product__product__supplier', 'product_order__product__product__name')

    def dispatch(self, request, sale_id, member_id, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        self.member = get_object_or_404(User, pk=member_id)
        return super(SaleDayMemberOrdersUpdateView, self).dispatch(request)

    def get_context_data(self, **kwargs):
        context = super(SaleDayMemberOrdersUpdateView, self).get_context_data(**kwargs)
        context.update({'sale': self.sale, 'member': self.member})
        return context

    def get_success_url(self):
        if '_redirect_to_summary' not in self.request.POST:
            return super(SaleDayMemberOrdersUpdateView, self).get_success_url()
        else:
            return reverse('member-orders-by-sale', kwargs={'sale_id': self.sale.id, 'member_id': self.member.id})
    
@class_view_decorator(never_cache)
class MemberOrdersView(TemplateView):
    '''
    Display member orders for a specific sale, and allow editing of payment details
    '''
    template_name = 'sale_day/member_orders_by_sale.html'
    
    def dispatch(self, request, sale_id, member_id, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        self.member = get_object_or_404(User, pk=member_id)
        self.orders = self.get_queryset()
        self.payment = Payment.objects.get(sale=self.sale, member=self.member)
        try:
            self.cheque_payment = self.payment.chequepayment
        except ObjectDoesNotExist:
            self.cheque_payment = None
        self.payment_form = PaymentForm(instance=self.payment)
        self.chequepayment_form = ChequePaymentForm(instance=self.cheque_payment)
        return super(MemberOrdersView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return ProductFinalOrder.objects.select_related().by_sale(self.sale).by_member(self.member).non_zero() \
            .order_by('product_order__product__product__supplier', 'product_order__product__product__name')

    def redirect_on_success(self):
        return HttpResponseRedirect(reverse('manage-sale', args=(self.sale.id,)))

    def handle_cash_payment(self):
        self.payment.save()
        return self.redirect_on_success()

    def handle_cheque_payment(self, request):
        '''
        Returns True if was able to validate and save payment in check.
        Assumes
        '''
        self.chequepayment_form = ChequePaymentForm(request.POST, instance=self.cheque_payment)
#        except ObjectDoesNotExist:
#            self.chequepayment_form = ChequePaymentForm(request.POST)
        if not self.chequepayment_form.is_valid():
            return False
        self.cheque_payment = self.chequepayment_form.save(commit=False)
        self.cheque_payment.payment = self.payment
        self.payment.save()
        self.cheque_payment.save()
        return True
    
    def post(self, request, *args, **kwargs):
        self.payment_form = PaymentForm(request.POST, instance=self.payment)
        if self.payment_form.is_valid():
            self.payment.updated_by = request.user
            if self.payment.method == METHOD_CASH:
                return self.handle_cash_payment()
            elif self.payment.method == METHOD_CHEQUE:
                if self.handle_cheque_payment(request):
                    return self.redirect_on_success()
        return self.render_to_response(self.get_context_data())
    
    def get_context_data(self, **kwargs):
        context = super(MemberOrdersView, self).get_context_data(**kwargs)
        context['sale']   = self.sale
        context['member'] = self.member
        context['orders'] = self.orders
        context['payment_form'] = self.payment_form
        context['chequepayment_form'] = self.chequepayment_form
        return context

@class_view_decorator(never_cache)
class ManageSaleChequesView(InlineFormSetView):
    '''
    List cheques received at a specific sale, and help divide them into envelopes
    '''
    model = Sale
    inline_model = ChequeEnvelope
    template_name = 'sale_day/manage_cheques.html'
    #exclude = ('selling_price', 'deleted')

    def get_context_data(self, **kwargs):
        context = super(ManageSaleChequesView, self).get_context_data(**kwargs)
        self.sale = get_object_or_404(Sale, pk=self.kwargs['pk'])
        context['sale'] = self.sale
        self.cheque_payments = ChequePayment.objects.by_sale(sale=self.sale)
        context['cheque_payments'] = self.cheque_payments
        return context

@class_view_decorator(never_cache)
class PriceListSheetView(View, CsvViewMixin):
    def get_header(self):
        return ['Product name', 'Supplier name', 'Unit', 'Packaged', 'Organic',
                'Package size', 'Description', 'Price']
        
    def get_row_values(self, sale_product):
        product = sale_product.product
        values = [product.name, product.supplier.name, product.unit.unit,
                  product.get_packaged_display(), product.get_organic_display(),
                  sale_product.package_size, product.description, sale_product.price]
        return values

    def get(self, request, sale_id, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        self.sale_products = self.sale.saleproduct_set.select_related().order_by('product__name').all()
        self.filename = 'pricelist-sale-%d.csv' %self.sale.id
        return self.render_to_csv_response(self.filename, *args, **kwargs)
    
    def calc_values(self, *args, **kwargs):
        values = [self.get_row_values(sale_product) for sale_product in self.sale_products]
        return values

@class_view_decorator(never_cache)
class TreasurySaleSummary(TemplateView):
    template_name = 'treasury/sale_summary.html'
    
    def get(self, request, sale_id, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        return super(TreasurySaleSummary, self).get(request, *args, **kwargs)

    def get_summary_data(self):
        sale_transfers = Transfer.objects.filter(sale=self.sale)
#        total_transfers_to_suppliers = abs(sale_transfers.transfers_to_suppliers().total())
        total_payments_to_suppliers = sum([payment.sum for payment in PaymentToSupplier.objects.filter(sale=self.sale)])
#        total_member_transfers = sale_transfers.member_transfers().total()
        total_member_payments = sum([payment.sum_paid for payment in Payment.objects.filter(sale=self.sale)])
        total_minus_selling_ratio = decimal.Decimal(total_member_payments / SELLING_PRICE_RATIO)
        total_profit = total_member_payments - total_payments_to_suppliers
        total_other_income = sale_transfers.other_income().total()
        total_other_expenses = abs(sale_transfers.other_expenses().total())
        total_profit = total_member_payments + total_other_income - total_payments_to_suppliers - total_other_expenses
        return locals()
 
    def get_context_data(self, **kwargs):
        context = super(TreasurySaleSummary, self).get_context_data(**kwargs)
        context.update({'sale': self.sale})
        context.update(self.get_summary_data())
        return context

@class_view_decorator(never_cache)
class TreasuryMemberFinalOrdersView(TemplateView):
    template_name = 'treasury/sale_member_final_orders.html'
    
    def get(self, request, sale_id, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        self.members = User.objects.non_admin()
        return super(TreasuryMemberFinalOrdersView, self).get(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(TreasuryMemberFinalOrdersView, self).get_context_data(**kwargs)
        context.update({'sale': self.sale, 'members': self.members})
        return context

@class_view_decorator(never_cache)
class TreasuryPostSaleSupplierSummariesView(TemplateView):
    template_name = 'treasury/post_sale_supplier_summaries.html'
    
    def get(self, request, sale_id, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        self.suppliers = Supplier.objects.all()
        return super(TreasuryPostSaleSupplierSummariesView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TreasuryPostSaleSupplierSummariesView, self).get_context_data(**kwargs)
        context.update({'sale': self.sale, 'suppliers': self.suppliers})
        return context

@class_view_decorator(never_cache)
class TreasuryUsefulLinks(TemplateView):
    template_name = 'treasury/useful_links.html'
    
    def get(self, request, sale_id, *args, **kwargs):
        self.sale = get_object_or_404(Sale, pk=sale_id)
        return super(TreasuryUsefulLinks, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TreasuryUsefulLinks, self).get_context_data(**kwargs)
        self.suppliers = Supplier.objects.all()
        self.members = User.objects.non_admin()
        context.update({'sale': self.sale, 'suppliers': self.suppliers, 'members': self.members})
        return context

class BaseArchive(TemplateView):
    def get_context_data(self, **kwargs):
        context = super(BaseArchive, self).get_context_data(**kwargs)
        context['sales'] = Sale.objects.all().order_by('-sale_date')
        context['archive_mode'] = False
        return context

@class_view_decorator(never_cache)
class AdminEditSuppliersProducts(BaseArchive):
    '''
    Show links to edit all supplier's products
    '''
    template_name = 'admin/edit_suppliers_products.html'

    def get_context_data(self, **kwargs):
        context = super(AdminEditSuppliersProducts, self).get_context_data(**kwargs)
        context['suppliers'] = Supplier.objects.all()
        return context

@class_view_decorator(never_cache)
class AdminArchive(BaseArchive):
    '''
    Show useful links to administrators
    '''
    template_name = 'archive/admin_archive.html'

    def get_context_data(self, **kwargs):
        context = super(AdminArchive, self).get_context_data(**kwargs)
        context['suppliers'] = Supplier.objects.all()
        return context

@class_view_decorator(never_cache)
class MemberArchive(BaseArchive):
    '''
    Show useful links to members, such as previuos orders
    '''
    template_name = 'archive/member_archive.html'

@class_view_decorator(never_cache)
class TreasuryArchive(BaseArchive):
    template_name = 'archive/treasury_archive.html'

    def get_context_data(self, **kwargs):
        context = super(TreasuryArchive, self).get_context_data(**kwargs)
        context.update({'members': User.objects.non_admin(),
                        'suppliers': Supplier.objects.all()})
        return context

@class_view_decorator(never_cache)
class PurchaseArchive(BaseArchive):
    '''
    Show links to pages related to past sales
    '''
    template_name = 'archive/purchase_archive.html'
    
    def get_context_data(self, **kwargs):
        context = super(PurchaseArchive, self).get_context_data(**kwargs)
        context['archive_mode'] = True
        return context

@class_view_decorator(never_cache)
class ImpersonateView(FormView):
    form_class = ChooseMemberForm
    template_name = 'admin/impersonate.html'
    
    def form_valid(self, form):
        member = form.cleaned_data['member']
        return HttpResponseRedirect(reverse('home') + '?__impersonate=%d' %member.id) 

@class_view_decorator(never_cache)
class ProductFinalOrderUpdateView(View):
    def post(self, request, *args, **kwargs):
        order_id = int(request.POST['order_id'])
        product_final_order = get_object_or_404(ProductFinalOrder, pk=order_id)
        product_final_order.was_bought = (request.POST['was_bought'] == 'true')
        product_final_order.save()
        return HttpResponse("ProductFinalOrder %d was updated successfully (was_bought set to %s)"
                            %(product_final_order.id, product_final_order.was_bought), mimetype="text/plain")

@class_view_decorator(never_cache)
class AdminSaleStatusView(TemplateView):
    template_name = 'admin/sale_status.html'

    def get(self, request, sale_id, *args, **kwargs):    
        self.sale = get_object_or_404(Sale, pk=sale_id)
        return super(AdminSaleStatusView, self).get(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(AdminSaleStatusView, self).get_context_data(**kwargs)
        context.update({
            'sale': self.sale,
            'categories': self.sale.get_product_statistics(),
            'order_count_per_member': ProductOrder.objects.select_related().by_sale(self.sale).non_zero().order_count_per_member(),
        })
        return context

@class_view_decorator(never_cache)
class ProductListView(ListView):
    class_name = Product
    queryset = Product.objects.filter(deleted=False).order_by('supplier__name', 'name')
    template_name = 'foodcoop/product_list.html'

@class_view_decorator(never_cache)
@class_view_decorator(login_required)
class ItemApiListView(APIView):
    serializer_class = None

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

    def save(self, json_data):
        queryset = self.get_queryset()
        serializer = self.serializer_class(queryset, data=json_data, many=True)
        if serializer.is_valid():
            serializer.save()
            return HttpResponse(json.dumps({'status': 'success', 'message': "saved data"}), "application/json")
        else:
            return HttpResponse(json.dumps({'status': 'error', 'errors': serializer.errors}), "application/json")

    def post(self, request, *args, **kwargs):
        json_data = json.loads(request.body).get('items', None)
        return self.save(json_data)

class OtherPaymentApiListView(ItemApiListView):
    serializer_class = OtherPaymentSerializer
    filter_fields = ('expense',)

    def get_queryset(self):
        return OtherPayment.objects.all()

@class_view_decorator(never_cache)
class InventoryListView(ListView):
    class_name = Product
#    queryset = Product.objects.filter(deleted=False).order_by('supplier', 'name')
    queryset = Product.objects.filter(inventory__gt = 0).order_by('supplier__name', 'name')
    template_name = 'foodcoop/product_list.html'

