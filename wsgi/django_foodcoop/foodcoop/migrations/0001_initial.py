# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UserProfile'
        db.create_table(u'foodcoop_userprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], unique=True)),
            ('user_number', self.gf('django.db.models.fields.IntegerField')(unique=True, null=True, blank=True)),
            ('joined_at', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('left_at', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('bought_share_at', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('share_price', self.gf('django.db.models.fields.DecimalField')(default=300, null=True, max_digits=10, decimal_places=2, blank=True)),
        ))
        db.send_create_signal(u'foodcoop', ['UserProfile'])

        # Adding model 'Person'
        db.create_table(u'foodcoop_person', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='persons', to=orm['auth.User'])),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
            ('home_phone_number', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('cellular_phone_number', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('email_address', self.gf('django.db.models.fields.EmailField')(max_length=75)),
        ))
        db.send_create_signal(u'foodcoop', ['Person'])

        # Adding model 'Sale'
        db.create_table(u'foodcoop_sale', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('sale_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('closing_date', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(default='open_to_buyers', max_length=15)),
            ('comments', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'foodcoop', ['Sale'])

        # Adding model 'Supplier'
        db.create_table(u'foodcoop_supplier', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'foodcoop', ['Supplier'])

        # Adding M2M table for field buyers on 'Supplier'
        db.create_table(u'foodcoop_supplier_buyers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('supplier', models.ForeignKey(orm[u'foodcoop.supplier'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(u'foodcoop_supplier_buyers', ['supplier_id', 'user_id'])

        # Adding model 'Unit'
        db.create_table(u'foodcoop_unit', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('unit', self.gf('django.db.models.fields.CharField')(max_length=25)),
        ))
        db.send_create_signal(u'foodcoop', ['Unit'])

        # Adding model 'Product'
        db.create_table(u'foodcoop_product', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('supplier', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foodcoop.Supplier'])),
            ('unit', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foodcoop.Unit'])),
            ('price_before_VAT', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2, blank=True)),
            ('price_including_VAT', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2, blank=True)),
            ('selling_price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2, blank=True)),
            ('packaged', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('organic', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('package_size', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('deleted', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'foodcoop', ['Product'])

        # Adding model 'SaleProduct'
        db.create_table(u'foodcoop_saleproduct', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foodcoop.Product'])),
            ('sale', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foodcoop.Sale'])),
            ('price', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('package_size', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
        ))
        db.send_create_signal(u'foodcoop', ['SaleProduct'])

        # Adding model 'ProductOrder'
        db.create_table(u'foodcoop_productorder', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('member', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foodcoop.SaleProduct'])),
            ('amount', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
        ))
        db.send_create_signal(u'foodcoop', ['ProductOrder'])

        # Adding model 'ProductFinalOrder'
        db.create_table(u'foodcoop_productfinalorder', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('product_order', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foodcoop.ProductOrder'])),
            ('member', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foodcoop.SaleProduct'])),
            ('amount', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
        ))
        db.send_create_signal(u'foodcoop', ['ProductFinalOrder'])

        # Adding model 'TransferType'
        db.create_table(u'foodcoop_transfertype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('expense', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('supplier', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foodcoop.Supplier'], blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], blank=True)),
        ))
        db.send_create_signal(u'foodcoop', ['TransferType'])

        # Adding model 'TransferMethod'
        db.create_table(u'foodcoop_transfermethod', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('method', self.gf('django.db.models.fields.CharField')(unique=True, max_length=25)),
        ))
        db.send_create_signal(u'foodcoop', ['TransferMethod'])

        # Adding model 'Transfer'
        db.create_table(u'foodcoop_transfer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foodcoop.TransferType'])),
            ('transfer_date', self.gf('django.db.models.fields.DateField')()),
            ('sum', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('identifier', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('method', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foodcoop.TransferMethod'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], blank=True)),
        ))
        db.send_create_signal(u'foodcoop', ['Transfer'])

        # Adding model 'Payment'
        db.create_table(u'foodcoop_payment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sale', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foodcoop.Sale'])),
            ('member', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('sum', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('sum_paid', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2)),
            ('took_order', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'foodcoop', ['Payment'])


    def backwards(self, orm):
        # Deleting model 'UserProfile'
        db.delete_table(u'foodcoop_userprofile')

        # Deleting model 'Person'
        db.delete_table(u'foodcoop_person')

        # Deleting model 'Sale'
        db.delete_table(u'foodcoop_sale')

        # Deleting model 'Supplier'
        db.delete_table(u'foodcoop_supplier')

        # Removing M2M table for field buyers on 'Supplier'
        db.delete_table('foodcoop_supplier_buyers')

        # Deleting model 'Unit'
        db.delete_table(u'foodcoop_unit')

        # Deleting model 'Product'
        db.delete_table(u'foodcoop_product')

        # Deleting model 'SaleProduct'
        db.delete_table(u'foodcoop_saleproduct')

        # Deleting model 'ProductOrder'
        db.delete_table(u'foodcoop_productorder')

        # Deleting model 'ProductFinalOrder'
        db.delete_table(u'foodcoop_productfinalorder')

        # Deleting model 'TransferType'
        db.delete_table(u'foodcoop_transfertype')

        # Deleting model 'TransferMethod'
        db.delete_table(u'foodcoop_transfermethod')

        # Deleting model 'Transfer'
        db.delete_table(u'foodcoop_transfer')

        # Deleting model 'Payment'
        db.delete_table(u'foodcoop_payment')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'foodcoop.payment': {
            'Meta': {'object_name': 'Payment'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'sale': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.Sale']"}),
            'sum': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'sum_paid': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'took_order': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'foodcoop.person': {
            'Meta': {'object_name': 'Person'},
            'cellular_phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'email_address': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'home_phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'persons'", 'to': u"orm['auth.User']"})
        },
        u'foodcoop.product': {
            'Meta': {'object_name': 'Product'},
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'organic': ('django.db.models.fields.SmallIntegerField', [], {}),
            'package_size': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'packaged': ('django.db.models.fields.SmallIntegerField', [], {}),
            'price_before_VAT': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'price_including_VAT': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'selling_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'supplier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.Supplier']"}),
            'unit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.Unit']"})
        },
        u'foodcoop.productfinalorder': {
            'Meta': {'object_name': 'ProductFinalOrder'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.SaleProduct']"}),
            'product_order': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.ProductOrder']"})
        },
        u'foodcoop.productorder': {
            'Meta': {'object_name': 'ProductOrder'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.SaleProduct']"})
        },
        u'foodcoop.sale': {
            'Meta': {'object_name': 'Sale'},
            'closing_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'comments': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sale_date': ('django.db.models.fields.DateTimeField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'open_to_buyers'", 'max_length': '15'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'foodcoop.saleproduct': {
            'Meta': {'object_name': 'SaleProduct'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package_size': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.Product']"}),
            'sale': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.Sale']"})
        },
        u'foodcoop.supplier': {
            'Meta': {'object_name': 'Supplier'},
            'buyers': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.User']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'foodcoop.transfer': {
            'Meta': {'object_name': 'Transfer'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'method': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.TransferMethod']"}),
            'sum': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'transfer_date': ('django.db.models.fields.DateField', [], {}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.TransferType']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'blank': 'True'})
        },
        u'foodcoop.transfermethod': {
            'Meta': {'object_name': 'TransferMethod'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'method': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '25'})
        },
        u'foodcoop.transfertype': {
            'Meta': {'object_name': 'TransferType'},
            'description': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'expense': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'supplier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.Supplier']", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'blank': 'True'})
        },
        u'foodcoop.unit': {
            'Meta': {'object_name': 'Unit'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        u'foodcoop.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'bought_share_at': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'joined_at': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'left_at': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'share_price': ('django.db.models.fields.DecimalField', [], {'default': '300', 'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'user_number': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['foodcoop']