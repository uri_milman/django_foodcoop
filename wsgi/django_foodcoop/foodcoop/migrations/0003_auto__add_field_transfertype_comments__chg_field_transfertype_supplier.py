# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'TransferType.comments'
        db.add_column(u'foodcoop_transfertype', 'comments',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)


        # Changing field 'TransferType.supplier'
        db.alter_column(u'foodcoop_transfertype', 'supplier_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foodcoop.Supplier'], null=True))

        # Changing field 'TransferType.user'
        db.alter_column(u'foodcoop_transfertype', 'user_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True))
        # Adding field 'Transfer.made_by'
        db.add_column(u'foodcoop_transfer', 'made_by',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='made_transfers', null=True, to=orm['auth.User']),
                      keep_default=False)


        # Renaming column for 'Transfer.method' to match new field type.
        db.rename_column(u'foodcoop_transfer', 'method_id', 'method')
        # Changing field 'Transfer.method'
        db.alter_column(u'foodcoop_transfer', 'method', self.gf('django.db.models.fields.CharField')(max_length=15))
        # Removing index on 'Transfer', fields ['method'] - commented out by Ophir
        #db.delete_index(u'foodcoop_transfer', ['method_id'])


    def backwards(self, orm):
        # Adding index on 'Transfer', fields ['method']
        db.create_index(u'foodcoop_transfer', ['method_id'])

        # Deleting field 'TransferType.comments'
        db.delete_column(u'foodcoop_transfertype', 'comments')


        # User chose to not deal with backwards NULL issues for 'TransferType.supplier'
        raise RuntimeError("Cannot reverse this migration. 'TransferType.supplier' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'TransferType.user'
        raise RuntimeError("Cannot reverse this migration. 'TransferType.user' and its values cannot be restored.")
        # Deleting field 'Transfer.made_by'
        db.delete_column(u'foodcoop_transfer', 'made_by_id')


        # Renaming column for 'Transfer.method' to match new field type.
        db.rename_column(u'foodcoop_transfer', 'method', 'method_id')
        # Changing field 'Transfer.method'
        db.alter_column(u'foodcoop_transfer', 'method_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foodcoop.TransferMethod']))

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'foodcoop.chequeenvelope': {
            'Meta': {'object_name': 'ChequeEnvelope'},
            'external_id': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'internal_id': ('django.db.models.fields.IntegerField', [], {}),
            'sale': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cheque_envelopes'", 'to': u"orm['foodcoop.Sale']"})
        },
        u'foodcoop.chequepayment': {
            'Meta': {'object_name': 'ChequePayment'},
            'account_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'account_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'bank': ('django.db.models.fields.IntegerField', [], {}),
            'cheque_number': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'envelope_internal_id': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'payment': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['foodcoop.Payment']", 'unique': 'True'})
        },
        u'foodcoop.payment': {
            'Meta': {'object_name': 'Payment'},
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'method': ('django.db.models.fields.CharField', [], {'default': "'cheque'", 'max_length': '15'}),
            'sale': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.Sale']"}),
            'sum': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'sum_paid': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'took_order': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'updated_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'updated_payments'", 'null': 'True', 'to': u"orm['auth.User']"})
        },
        u'foodcoop.person': {
            'Meta': {'object_name': 'Person'},
            'cellular_phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'email_address': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'home_phone_number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'persons'", 'to': u"orm['auth.User']"})
        },
        u'foodcoop.product': {
            'Meta': {'object_name': 'Product'},
            'deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'organic': ('django.db.models.fields.SmallIntegerField', [], {}),
            'package_size': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'packaged': ('django.db.models.fields.SmallIntegerField', [], {}),
            'price_before_VAT': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'price_including_VAT': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'selling_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'supplier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.Supplier']"}),
            'unit': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.Unit']"})
        },
        u'foodcoop.productfinalorder': {
            'Meta': {'object_name': 'ProductFinalOrder'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'product_order': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.ProductOrder']"})
        },
        u'foodcoop.productorder': {
            'Meta': {'object_name': 'ProductOrder'},
            'amount': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.SaleProduct']"})
        },
        u'foodcoop.sale': {
            'Meta': {'object_name': 'Sale'},
            'closing_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'comments': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sale_date': ('django.db.models.fields.DateTimeField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'open_to_buyers'", 'max_length': '15'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'foodcoop.saleproduct': {
            'Meta': {'object_name': 'SaleProduct'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package_size': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.Product']"}),
            'sale': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.Sale']"})
        },
        u'foodcoop.salestaffmember': {
            'Meta': {'object_name': 'SaleStaffMember'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'sale': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'staff_members'", 'to': u"orm['foodcoop.Sale']"})
        },
        u'foodcoop.supplier': {
            'Meta': {'object_name': 'Supplier'},
            'buyers': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.User']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'foodcoop.transfer': {
            'Meta': {'object_name': 'Transfer'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'made_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'made_transfers'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'method': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'sum': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'transfer_date': ('django.db.models.fields.DateField', [], {}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.TransferType']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'blank': 'True'})
        },
        u'foodcoop.transfermethod': {
            'Meta': {'object_name': 'TransferMethod'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'method': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '25'})
        },
        u'foodcoop.transfertype': {
            'Meta': {'object_name': 'TransferType'},
            'comments': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'expense': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'supplier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foodcoop.Supplier']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        u'foodcoop.unit': {
            'Meta': {'object_name': 'Unit'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        u'foodcoop.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'bought_share_at': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'joined_at': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'left_at': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'share_price': ('django.db.models.fields.DecimalField', [], {'default': '300', 'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'user_number': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['foodcoop']