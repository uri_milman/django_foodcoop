from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError

from foodcoop.utils import FIXTURES

class Command(BaseCommand):
    args = 'None'
    help = 'Initializes database in code'
    
    def load_fixtures(self):
        '''Load data from various fixtures to populate database'''
        for fixture in FIXTURES:
            call_command('loaddata', fixture)
            self.stdout.write('Successfully loaded fixture %s' %fixture)

    def handle(self, *args, **options):
        call_command('syncdb', interactive=False)
        call_command('migrate', 'django_extensions')
        call_command('migrate', 'reversion')
        call_command('migrate', 'foodcoop')
        self.load_fixtures()
