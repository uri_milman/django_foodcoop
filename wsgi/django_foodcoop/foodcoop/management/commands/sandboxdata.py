import random

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError

from foodcoop.models import Person

ORDERED_GROUP_NAMES = ('Packers', 'Purchasing managers', 'Receivers', 'Sale day managers', 'Treasurers', 'Customers')

GROUP_NAME_SHORTCUTS = dict((
    ('Packers', 'packer'),
    ('Purchasing managers', 'purchasemanager'),
    ('Receivers', 'receiver'),
    ('Sale day managers', 'salemanager'),
    ('Treasurers', 'treasurer'),
    ('Customers', 'customer'),
))

DEFAULT_PASSWORD = 'sand1357'

class Command(BaseCommand):
    args = 'None'
    help = 'Changes production data to avoid confusion between sandbox and production sites'

    def modify_user(self, user, new_usernames_helper):
        old_username = user.username
        new_username = None
        if user.username == 'admin':
            new_username = 'admin'
        else:
            user_groups = user.groups.values_list('name', flat=True)
            for group_name in ORDERED_GROUP_NAMES:
                if group_name in user_groups:
                    new_username = '%s%d' %(GROUP_NAME_SHORTCUTS[group_name], new_usernames_helper[group_name])
                    new_usernames_helper[group_name] += 1
                    break
        if new_username is None:
            raise Exception('Could not rename user %s with groups %s' %(user, user_groups))
        user.username = new_username
        user.first_name = new_username
        user.last_name = 'cohen'
        user.email = '%s@food.coop' %new_username
        user.set_password(DEFAULT_PASSWORD)
        user.save()
        print('Renamed user %s to %s, set default password to %s' %(old_username, new_username, DEFAULT_PASSWORD))

    def modify_person(self, person, person_number):
#        print person.name
        person.name = 'person%d' %person_number
        person.email_address = person.user.email
        person.home_phone_number = '08-%s' %random.randint(100000, 999999)
        person.cellular_phone_number = '059-%s' %random.randint(100000, 999999)
        person.save()
        print(u'Renamed person %d to %s' %(person_number, person.name))

    def handle(self, *args, **options):
        new_usernames_helper = dict([(group_name, 1) for group_name in ORDERED_GROUP_NAMES])
        for user in User.objects.all():
            self.modify_user(user, new_usernames_helper)
        person_number = 1
        for person in Person.objects.all():
            self.modify_person(person, person_number)
            person_number += 1
