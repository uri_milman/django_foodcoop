from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required # TODO: find a better way to protect views
#from django.views.generic.base import RedirectView
from django.views.decorators.csrf import csrf_exempt # TODO: get rid of this

from foodcoop.views import *

urlpatterns = patterns('foodcoop.views',
    # admin
    url(r'^actions/sale/(?P<sale_id>\d+)/draft/$', 'sale_draft', name='draft-sale'),
    url(r'^actions/sale/(?P<sale_id>\d+)/open/$', 'sale_open', name='open-sale'),
    url(r'^actions/sale/(?P<sale_id>\d+)/finalize/$', 'sale_finalize', name='finalize-sale'),
    url(r'^actions/sale/(?P<sale_id>\d+)/close/$', 'sale_close', name='close-sale'),
    url(r'^impersonate/$', login_required(ImpersonateView.as_view()), name='impersonate'),
    url(r'^admin/sale/(?P<sale_id>\d+)/status/$', login_required(AdminSaleStatusView.as_view()), name='admin-sale-status'),
    
    # general
    url(r'^members/$', login_required(MembersListView.as_view()), name='member-list'),
    url(r'^products/$', login_required(ProductListView.as_view()), name='product-list'),
    url(r'^inventory/$', login_required(InventoryListView.as_view()), name='inventory-list'),
    
    # members
    url(r'^sale/(?P<sale_id>\d+)/order/$', login_required(MemberOrderFormView.as_view()), name='member-order-form'),
    url(r'^sale/(?P<pk>\d+)/summary/$', login_required(MemberOrderListView.as_view()), name='member-order-list'),
    url(r'^s/(?P<sale_id>\d+)/m/(?P<member_id>\d+)/final/$', login_required(MemberFinalOrderListView.as_view()), name='member-final-order-list'),
    url(r'^s/(?P<sale_id>\d+)/m/(?P<member_id>\d+)/epilogue/$', login_required(MemberPostOrderListView.as_view()), name='member-post-order-list'),
    url(r'^member/archive/$', login_required(MemberArchive.as_view()), name='member-archive'),
    
    # purchasing management
    url(r'^supplier/(?P<pk>\d+)/$', login_required(SupplierProductListView.as_view()), name='supplier-products'),
    url(r'^supplier/(?P<pk>\d+)/category/(?P<category_id>\d+)/$', login_required(SupplierProductListView.as_view()), name='select-category-products'),
    url(r'^supplier/(?P<pk>\d+)/edit/$', login_required(EditSupplierProductsView.as_view()), name='edit-supplier-products'),
    url(r'^category/(?P<pk>\d+)/edit/$', login_required(EditSupplierCategoryProductsView.as_view()), name='edit-category-products'),
    url(r'^supplier/(?P<pk>\d+)/update/$', csrf_exempt(UpdateSupplierSaleProductsView.as_view()), name='update-supplier-sale-products'),
    url(r'^sale/(?P<sale_id>\d+)/supplier/(?P<supplier_id>\d+)/$', login_required(PurchaseMemberOrdersUpdateView.as_view()), name='purchase-member-orders-form-by-supplier'),
    url(r'^sale/(?P<sale_id>\d+)/category/(?P<category_id>\d+)/$', login_required(PurchaseMemberOrdersUpdateView.as_view()), name='purchase-member-orders-form-by-category'),
    url(r'^sale/(?P<sale_id>\d+)/supplier/(?P<supplier_id>\d+)/summary/$', login_required(SupplierSaleOrderSummaryView.as_view()), name='supplier-sale-order-summary'),
    url(r'^sale/(?P<sale_id>\d+)/supplier/(?P<supplier_id>\d+)/summary.csv$', login_required(SupplierSaleOrderSummaryCsvView.as_view()), name='supplier-sale-order-csv-summary'),
    url(r'^sale/(?P<sale_id>\d+)/supplier/(?P<supplier_id>\d+)/epilogue/$', login_required(SupplierSaleProductListSummaryFormView.as_view()), name='supplier-sale-post-order-summary'),
    url(r'^sale/(?P<sale_id>\d+)/supplier/(?P<supplier_id>\d+)/epilogue/readonly/$', login_required(SupplierSaleProductListSummaryView.as_view()), name='supplier-sale-post-order-summary-readonly'),
    url(r'^draft/(?P<sale_id>\d+)/supplier/(?P<supplier_id>\d+)/update/$', login_required(SupplierSaleProductListFormView.as_view()), name='supplier-sale-product-list-form'),
    
    # treasury
    url(r'^shares/$', login_required(SharesListView.as_view()), name='share-list'),
    url(r'^transfers/$', login_required(TransfersListView.as_view()), name='transfer-list'),
    url(r'^transfer/new/$', login_required(CreateTransferView.as_view()), name='create-transfer'),
    url(r'^transfer/(?P<pk>\d+)/$', login_required(UpdateTransferView.as_view()), name='edit-transfer'),
    url(r'^transfer/(?P<pk>\d+)/delete/$', login_required(DeleteTransferView.as_view()), name='delete-transfer'),
    url(r'^payments/$', login_required(PaymentsBreakdown.as_view()), name='payments-breakdown'),
    url(r'^payments/member/(?P<member_id>\d+)/$', login_required(MemberPaymentsListView.as_view()), name='payments-by-member'),
    url(r'^payments/sale/(?P<sale_id>\d+)/$', login_required(SalePaymentsListView.as_view()), name='payments-by-sale'),
    url(r'^sale/(?P<sale_id>\d+)/useful-links/$', login_required(TreasuryUsefulLinks.as_view()), name='treasury-useful-links'),
    url(r'^treasury/archive/$', login_required(TreasuryArchive.as_view()), name='treasury-archive'),
    url(r'^treasury/sale/(?P<sale_id>\d+)/summary/$', login_required(TreasurySaleSummary.as_view()), name='treasury-sale-summary'),
    url(r'^treasury/sale/(?P<sale_id>\d+)/member-orders/$', login_required(TreasuryMemberFinalOrdersView.as_view()), name='treasury-sale-member-orders'),
    url(r'^treasury/sale/(?P<sale_id>\d+)/supplier-summaries/$', login_required(TreasuryPostSaleSupplierSummariesView.as_view()), name='treasury-sale-supplier-summaries'),

    # sale day managers
    url(r'^sale/(?P<sale_id>\d+)/manage/$', login_required(ManageSaleDayView.as_view()), name='manage-sale'),
    url(r'^sale/(?P<sale_id>\d+)/manage/summary/$', login_required(SaleDaySummaryView.as_view()), name='sale-day-summary'),
    url(r'^sale/(?P<pk>\d+)/cheques/$', login_required(ManageSaleChequesView.as_view()), name='manage-cheques'),
    url(r'^sale/(?P<sale_id>\d+)/member-notes/$', login_required(NotesPerMemberExcelView.as_view()), name='sale-day-member-notes'),
    url(r'^sale/(?P<sale_id>\d+)/product-notes/$', login_required(NotesPerProductExcelView.as_view()), name='sale-day-product-notes'),
    url(r'^s/(?P<sale_id>\d+)/m/(?P<member_id>\d+)/$', login_required(MemberOrdersView.as_view()), name='member-orders-by-sale'),
    url(r'^s/(?P<sale_id>\d+)/m/(?P<member_id>\d+)/update/$', login_required(SaleDayMemberOrdersUpdateView.as_view()), name='sale-day-member-order-update'),
    url(r'^pfo/update/$', login_required(ProductFinalOrderUpdateView.as_view()), name='product-final-order-update'),
    url(r'^sale/(?P<sale_id>\d+)/products/$', login_required(ProductsInSaleView.as_view()), name='products-in-sale'),
    url(r'^sale/(?P<sale_id>\d+)/product/(?P<saleproduct_id>\d+)/$', login_required(ProductSaleView.as_view()), name='product-in-sale'),
    
    # utilities
    url(r'^sale/(?P<sale_id>\d+)/pricelist.csv', login_required(PriceListSheetView.as_view()), name='price-list'),

    # archive
    url(r'^purchase/archive/$', login_required(PurchaseArchive.as_view()), name='purchase-archive'),
    url(r'^admin/archive/$', login_required(AdminArchive.as_view()), name='admin-archive'),
    url(r'^admin/products/edit/$', login_required(AdminEditSuppliersProducts.as_view()), name='admin-edit-suppliers-products'),

    # api
#    url(r'^api/otherpayments/$', OtherPaymentApiListView.as_view(), name="other-payments-api"),
)
