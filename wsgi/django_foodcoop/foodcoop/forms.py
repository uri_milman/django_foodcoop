# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.forms import models, Form, ModelForm
from django.forms.widgets import TextInput
from django.forms.extras.widgets import SelectDateWidget

from foodcoop.models import Payment, ChequePayment, Transfer, ProductOrder, SaleProduct, \
                            ProductFinalOrder, Product, SupplierCategory, PaymentToSupplier

__all__ = (
    'PaymentForm', 'ChequePaymentForm', 'TransferForm', 'ProductOrderForm', 'SaleProductForm',
    'ProductFinalOrderForm', 'PostOrderSaleProductForm', 'SaleDayProductFinalOrderForm', 'ChooseMemberForm',
    'ProductForm', 'ProductFormSet', 'PaymentToSupplierForm',
)

class PaymentForm(ModelForm):
    class Meta:
        model = Payment

class PaymentToSupplierForm(ModelForm):
    class Meta:
        model = PaymentToSupplier
        fields = ('sum', 'comments',)

class ChequePaymentForm(ModelForm):
    class Meta:
        model = ChequePayment

class TransferForm(ModelForm):
    class Meta:
        model = Transfer
        #widgets = {'transfer_date': SelectDateWidget()}

class ProductForm(ModelForm):
    class Meta:
        model = Product
        exclude = ('selling_price', 'price_before_VAT', 'deleted')
    
    def __init__(self, category_choices=None, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields['category'].choices = category_choices

class ProductFormSet(models.BaseInlineFormSet):
    def _construct_forms(self):
        self.forms = []
        category_choices = SupplierCategory.objects.all()
        form_defaults = {'category_choices': category_choices}

        for i in xrange(min(self.total_form_count(), self.absolute_max)):
            self.forms.append(self._construct_form(i, **form_defaults))

class ProductFinalOrderForm(ModelForm):
    class Meta:
        model = ProductFinalOrder
        fields = ('amount',)

class SaleDayProductFinalOrderForm(ModelForm):
    class Meta:
        model = ProductFinalOrder
        fields = ('amount_bought',)
        widgets = {
            'amount_bought': TextInput(attrs={'class': 'product_quantity'}), 
        }
    
class SaleProductForm(ModelForm):
    class Meta:
        model = SaleProduct
        fields = ('price_including_VAT', 'pre_sale_comments')
    
class PostOrderSaleProductForm(ModelForm):
    class Meta:
        model = SaleProduct
        fields = ('delivery_amount', 'amount_received',  'post_sale_comments',)
        widgets = {
            'delivery_amount': TextInput(attrs={'class': 'product_quantity'}), 
            'amount_received': TextInput(attrs={'class': 'product_quantity'}), 

        }
    
class ProductOrderForm(ModelForm):
    class Meta:
        model = ProductOrder
        exclude = ('member', 'product')
        widgets = {
            'min_amount': TextInput(attrs={'class':'conditional_disable'}), 
            'max_amount': TextInput(attrs={'class':'conditional_disable'}), 
        }
        
    def clean(self):
        '''
        Add restrictions on form input under the following circumstances:
        1. user entered a non-zero amount
        2. product's package size is not 1
        
        The restrictions are:
        1. min_amount <= amount <= max_amount
        2. max_amount - min_amount >= 1
        '''
        cleaned_data = super(ProductOrderForm, self).clean()
        amount = cleaned_data.get('amount')
        min_amount = cleaned_data.get('min_amount')
        max_amount = cleaned_data.get('max_amount')
        product = self.instance.product
        if amount is not None and product is not None and min_amount is not None and max_amount is not None \
                                and amount > 0 and product.product.package_size > 1:
            prev_errors = False
            if min_amount > amount:
                self._errors['min_amount'] = self.error_class(['כמות מינימלית אינה יכולה להיות גדולה מהכמות הרצויה'])
                del cleaned_data['min_amount']
                prev_errors = True
            if max_amount < amount:
                self._errors['max_amount'] = self.error_class(['כמות מקסימלית אינה יכולה להיות קטנה מהכמות הרצויה'])
                del cleaned_data['max_amount']
                prev_errors = True
            if not prev_errors and (min_amount + 1 > max_amount):
                error_message = "ההפרש בין הכמות המקסימלית לכמות המינימלית צריך להיות 1 לפחות"
                self._errors['min_amount'] = self.error_class([error_message])
                self._errors['max_amount'] = self.error_class([error_message])
                del cleaned_data['min_amount']
                del cleaned_data['max_amount']
        return cleaned_data

class ChooseMemberForm(Form):
    member = models.ModelChoiceField(queryset=User.objects.all())
